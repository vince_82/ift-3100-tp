/**
 *
 *  Web Starter Kit
 *  Copyright 2014 Google Inc. All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 *
 */

'use strict';

// Include Gulp & Tools We'll Use
var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var del = require('del');
var runSequence = require('run-sequence');
var browserSync = require('browser-sync');
var pagespeed = require('psi');
var reload = browserSync.reload;

var AUTOPREFIXER_BROWSERS = [
    'ie >= 10',
    'ie_mob >= 10',
    'ff >= 30',
    'chrome >= 34',
    'safari >= 7',
    'opera >= 23',
    'ios >= 7',
    'android >= 4.4',
    'bb >= 10'
];

// Bower

var bowerFiles = require('main-bower-files');
var inject = require('gulp-inject');
var es = require('event-stream');
var series = require('stream-series')

// Copy bower files into lib directory.
gulp.task('bower', function() {
    return gulp.src(bowerFiles(), {
            base: "bower_components",
        })
        .pipe(gulp.dest('./app/lib/'))
});

gulp.task('bower-not-jquery', function() {
    return gulp.src(bowerFiles(['!(jquery)*.js']), {
            base: "bower_components",
        })
        .pipe(gulp.dest('./app/lib/'))
});

// File injection in index.html

var namespaces = gulp.src(['app/scripts/namespaces/*.js'], {
    base: 'app',
    read: false
});
var base = gulp.src(['app/scripts/base/*.js'], {
    base: 'app',
    read: false
});
var componentsBase = gulp.src(['app/scripts/components/base/*.js'], {
    base: 'app',
    read: false
});
var components = gulp.src(['app/scripts/components/*.js'], {
    base: 'app',
    read: false
});
var geometry = gulp.src(['app/scripts/components/geometry/*.js'], {
    base: 'app',
    read: false
});
var math = gulp.src(['app/scripts/components/math/*.js'], {
    base: 'app',
    read: false
});
var utils = gulp.src(['app/scripts/utils/*.js'], {
    base: 'app',
    read: false
});
var main = gulp.src(['app/scripts/*.js'], {
    base: 'app',
    read: false
});
var notjquery = gulp.src(['app/lib/!(jquery)*/**/*.js'], {
    base: 'app',
    read: false
});
var jquery = gulp.src(['app/lib/jquery/**/*.js'], {
    base: 'app',
    read: false
});
var jquerydep = gulp.src(['app/lib/jquery-*/*.js'], {
    base: 'app',
    read: false
});
var models = gulp.src(['app/scripts/components/models/*.js'], {
    base: 'app',
    read: false
});
var uniforms = gulp.src(['app/scripts/components/shaders/uniforms/*.js'], {
    base: 'app',
    read: false
});
var shaders = gulp.src(['app/scripts/components/shaders/*.js'], {
    base: 'app',
    read: false
});

var game = gulp.src(['app/scripts/components/game/*.js'], {
    base: 'app',
    read: false
});
gulp.task('inject', ['bower'], function() {
    gulp.src('app/index.html')
        .pipe(inject(series(jquery, jquerydep, notjquery), {
            name: 'bower',
            ignorePath: 'app'
        }))
        .pipe(inject(
            series(namespaces, base, utils, math, componentsBase, components, geometry, main, models, uniforms, shaders, game), {
                ignorePath: 'app'
            }))
        .pipe(gulp.dest('./app'))
});

// Lint JavaScript
gulp.task('jshint', function() {
    return gulp.src('app/scripts/**/*.js')
        .pipe(reload({
            stream: true,
            once: true
        }))
        .pipe($.jshint())
        .pipe($.jshint.reporter('jshint-stylish'))
        .pipe($.if(!browserSync.active, $.jshint.reporter('fail')));
});

// Optimize Images
gulp.task('images', function() {
    return gulp.src('app/images/**/*')
        // .pipe($.cache($.imagemin({
        //     progressive: true,
        //     interlaced: true
        // })))
        .pipe(gulp.dest('dist/images'))
        // .pipe($.size({
        //     title: 'images'
        // }));
});

// Copy All Files At The Root Level (app)
gulp.task('copy', function() {
    return gulp.src([
            'app/*',
            '!app/*.html',
            'node_modules/apache-server-configs/dist/.htaccess'
        ], {
            dot: true
        }).pipe(gulp.dest('dist'))
        .pipe($.size({
            title: 'copy'
        }));
});

// Copy Web Fonts To Dist
gulp.task('fonts', function() {
    return gulp.src(['app/fonts/**'])
        .pipe(gulp.dest('dist/fonts'))
        .pipe($.size({
            title: 'fonts'
        }));
});

// Compile and Automatically Prefix Stylesheets
gulp.task('styles', function() {
    // For best performance, don't add Sass partials to `gulp.src`
    return gulp.src([
            'app/styles/*.css' //,
            // 'app/styles/**/*.css',
            // 'app/styles/components/components.scss'
        ])
        .pipe($.changed('styles', {
            extension: '.scss'
        }))
        // .pipe($.rubySass({
        //     style: 'expanded',
        //     precision: 10
        // }))
        .on('error', console.error.bind(console))
        .pipe($.autoprefixer({
            browsers: AUTOPREFIXER_BROWSERS
        }))
        .pipe(gulp.dest('.tmp/styles'))
        // Concatenate And Minify Styles
        .pipe($.if('*.css', $.csso()))
        .pipe(gulp.dest('dist/styles'))
        .pipe($.size({
            title: 'styles'
        }));
});

// Scan Your HTML For Assets & Optimize Them
gulp.task('html', function() {
    var assets = $.useref.assets({
        searchPath: '{.tmp,app}'
    });

    return gulp.src('app/**/*.html')
        .pipe(assets)
        // Concatenate And Minify JavaScript
        // .pipe($.if('*.js', $.uglify({
        //     preserveComments: 'some'
        // })))
        // Remove Any Unused CSS
        // Note: If not using the Style Guide, you can delete it from
        // the next line to only include styles your project uses.
        .pipe($.if('*.css', $.uncss({
            html: [
                'app/index.html',
            ],
            // CSS Selectors for UnCSS to ignore
            ignore: [
                /.navdrawer-container.open/,
                /.app-bar.open/
            ]
        })))
        // Concatenate And Minify Styles
        // In case you are still using useref build blocks
        .pipe($.if('*.css', $.csso()))
        .pipe(assets.restore())
        .pipe($.useref())
        // Update Production Style Guide Paths
        .pipe($.replace('components/components.css', 'components/main.min.css'))
        // Minify Any HTML
        // .pipe($.if('*.html', $.minifyHtml()))
        // Output Files
        .pipe(gulp.dest('dist'))
        .pipe($.size({
            title: 'html'
        }));
});

// Clean Output Directory
gulp.task('clean', del.bind(null, ['.tmp', 'dist']));

// Watch Files For Changes & Reload
gulp.task('serve', ['styles'], function() {
    runSequence('bower', 'inject');
    browserSync({
        notify: false,
        // Run as an https by uncommenting 'https: true'
        // Note: this uses an unsigned certificate which on first access
        //       will present a certificate warning in the browser.
        // https: true,
        server: {
            baseDir: ['.tmp', 'app']
        }
    });

    gulp.watch(['app/**/*.html'], reload);
    gulp.watch(['app/styles/**/*.{scss,css}'], ['styles', reload]);
    gulp.watch(['app/scripts/**/*.js'], ['jshint']);
    gulp.watch(['app/images/**/*'], reload);
});

// Build and serve the output from the dist build
gulp.task('serve:dist', ['default'], function() {
    browserSync({
        notify: false,
        // Run as an https by uncommenting 'https: true'
        // Note: this uses an unsigned certificate which on first access
        //       will present a certificate warning in the browser.
        // https: true,
        server: 'dist'
    });
});

// Build Production Files, the Default Task
gulp.task('default', ['clean'], function(cb) {
    runSequence('bower', 'inject');
    runSequence('styles', ['html', 'fonts', 'images', 'copy'], cb);
});

// Run PageSpeed Insights
// Update `url` below to the public URL for your site
gulp.task('pagespeed', pagespeed.bind(null, {
    // By default, we use the PageSpeed Insights
    // free (no API key) tier. You can use a Google
    // Developer API key if you have one. See
    // http://goo.gl/RkN0vE for info key: 'YOUR_API_KEY'
    url: 'https://example.com',
    strategy: 'mobile'
}));

// Load custom tasks from the `tasks` directory
try {
    require('require-dir')('tasks');
} catch (err) {}
