// Copy the attributes from source to dest, and
// update those already present.
// Return the modified object.
APP.Utils.copyToAndUpdate = function(dest, source) {
    return _.extend(dest, _.clone(source));
}

// Source : http://stackoverflow.com/questions/4554252/typed-arrays-in-gecko-2-float32array-concatenation-and-expansion
// Concat two float32 arrays and return a new one from that operation.
APP.Utils.Float32Concat = function(first, second) {
    var firstLength = first.length,
        result = new Float32Array(firstLength + second.length);

    result.set(first);
    result.set(second, firstLength);

    return result;
}

APP.Utils.Float32ConcatMultiple = function(arrays) {
    // First get total length.
    var totalLen = _.reduce(arrays, function(acc, a) {
        return acc + a.length;
    }, 0);

    // Declare a new array of totalLen elements.
    var result = new Float32Array(totalLen);

    // Concatenate each array in result.
    _.reduce(arrays, function(acc, a) {
        result.set(a, acc); // Set elements of a in result with offset acc.
        return acc + a.length; // Return the next offset.
    }, 0);

    return result;
}

APP.Utils.convertWebGLColorToRGB = function(value) {

    return [
        Math.floor(value[0] * 255),
        Math.floor(value[1] * 255),
        Math.floor(value[2] * 255)
    ];

}

APP.Utils.convertRGBToWebGLColor = function(color) {

    var r, g, b;

    if (_.isString(color) && /^#/.test(color)) {
        // Hexadecimal form.
        var rgb = color.slice(1); // Remove the #
        r = parseInt('0x'.concat(rgb.slice(0, 2))) / 255;
        g = parseInt('0x'.concat(rgb.slice(2, 4))) / 255;
        b = parseInt('0x'.concat(rgb.slice(4))) / 255;
    } else {
        // RGB array.
        r = color[0] / 255;
        g = color[1] / 255;
        b = color[2] / 255;
    }

    return vec3.fromValues(
        r,
        g,
        b
    );

}

APP.Utils.defineStandardProperties = function(obj, propertyValuePairs) {

    _.each(propertyValuePairs, function(propVal, prop) {

        Object.defineProperty(obj, prop, {
            get: function() {
                return propertyValuePairs[prop];
            },
            set: function(value) {
                propertyValuePairs[prop] = value;
            }
        });

    });

}
