//ParametricTerrainBuilder
// Builds an horizontal parametric surface. Can be textured or colored
// returns a group (object3d) of parametric surfaces
APP.ParametricTerrainBuilder = function(params) {
    var defaultColor = vec4.fromValues(0.0, 0.0, 0.0, 0.0);
    var defaults = {
        start: -10, //Z initial
        finish: 400, //Z final
        width: 80, // X interval
        height: -2, //Y level
        detailLevel: 3, //Defines the amount of rectangles needed for the plane. More rectangles = better texture but less performance.
        color: defaultColor, // No color by default.
        slices: 8,
        stacks: 8,
        controlPointsColor: vec4.fromValues(1.0, 0.0, 0.0, 1.0), //red.
        maxYLevel: 0.1,
        minYLevel: 0,
        randomOverride: 1, //can cancel randomness
        textureBase64String: undefined,
        textureLocation: {
            key: 'grass',
            type: 'groundtexture',
            file: 'Base64GroundTexture'
        },
        // strTextureImage: APP.Base64GroundTexture.groundtexture["grass"],
        // image: new Image(), //It is not not good to make oo many new images so this image will be reused

    };

    var _attrs = _.extend(defaults, _.clone(params));
    //APP.loadOBJ.initTexture(strTextureImage);

    //Parameters
    var start = _attrs.start;
    var finish = _attrs.finish;
    var width = _attrs.width;
    var height = _attrs.height;
    var detailLevel = _attrs.detailLevel;
    var color = _attrs.color;
    var slices = _attrs.slices;
    var stacks = _attrs.stacks;
    var controlPointsColor = _attrs.controlPointsColor;
    var stacks = _attrs.stacks;
    var maxYLevel = _attrs.maxYLevel;
    var minYLevel = _attrs.minYLevel;
    // var strTextureImage = _attrs.strTextureImage;
    var texLocation = _attrs.textureLocation;
    var texString = _attrs.textureBase64String;
    // var image = _attrs.image;
    var randomOverride = _attrs.randomOverride;

    // console.log(strTextureImage);

    //corners of the grid
    //var cornerSW = vec3.fromValues(width / 2, height, start);
    //var cornerSE = vec3.fromValues(-width / 2, height, start);
    //var cornerNW = vec3.fromValues(width / 2, height, finish);
    //var cornerNE = vec3.fromValues(-width / 2, height, finish);


    //Fill the plane grid with vertices, UV, indices and normal lists
    var cpt = 0;
    var verticesList = [];
    var bezierXList = [];
    var bezierZList = [];
    var coonsEquation;
    var groupTerrainSurfaces = new APP.Object3D();
    // console.log(Math.pow(2, detailLevel));
    // STORE ALL BEZIERS (LINES ON THE GRID (TOP VIEW))
    for (var i = 0; i <= Math.pow(2, detailLevel); i++) {
        for (var j = 0; j <= Math.pow(2, detailLevel); j++) {

            //Store points on the grid
            var gridX = width / 2 - j * width / Math.pow(2, detailLevel);
            var gridY = height + (maxYLevel) * (Math.random() * randomOverride) + minYLevel;
            var gridZ = i * (finish - start) / Math.pow(2, detailLevel);
            var offsetXZ = ((Math.random() * randomOverride) * 0.2 - (Math.random() * randomOverride) * 0.4);
            verticesList.push(vec3.fromValues(gridX + offsetXZ * width / Math.pow(2, detailLevel), gridY, start + gridZ + offsetXZ * ((finish - start) / Math.pow(2, detailLevel))));



            if (j > 0) {
                //Store beziers lines on X-AXIS
                bezierXList.push(new APP.Math.Curves.BezierCubic({
                    //Control points
                    P0: verticesList[cpt - 1],
                    P1: vec3.fromValues(verticesList[cpt - 1][0] - (width / Math.pow(2, detailLevel)) * (Math.random() * randomOverride) / 2,
                        verticesList[cpt - 1][1] + (maxYLevel - minYLevel) * (Math.random() * randomOverride) + minYLevel,
                        verticesList[cpt - 1][2]),
                    P2: vec3.fromValues(verticesList[cpt][0] + (width / Math.pow(2, detailLevel)) * (Math.random() * randomOverride) / 2,
                        verticesList[cpt][1] + (maxYLevel - minYLevel) * (Math.random() * randomOverride) + minYLevel,
                        verticesList[cpt][2]),
                    P3: verticesList[cpt]
                }));
            }
            if (i > 0) {
                //Store beziers lines on Z-AXIS
                bezierZList.push(new APP.Math.Curves.BezierCubic({
                    //Control points
                    P0: verticesList[cpt - Math.pow(2, detailLevel) - 1],
                    P1: vec3.fromValues(verticesList[cpt - Math.pow(2, detailLevel) - 1][0], verticesList[cpt - Math.pow(2, detailLevel) - 1][1] + (maxYLevel - minYLevel) * (Math.random() * randomOverride) + minYLevel, verticesList[cpt - Math.pow(2, detailLevel) - 1][2] + ((finish - start) / Math.pow(2, detailLevel)) * (Math.random() * randomOverride) / 2),
                    P2: vec3.fromValues(verticesList[cpt][0], verticesList[cpt][1] + (maxYLevel - minYLevel) * (Math.random() * randomOverride) + minYLevel, verticesList[cpt][2] - ((finish - start) / Math.pow(2, detailLevel)) * (Math.random() * randomOverride) / 2),
                    P3: verticesList[cpt]
                }));


                //groupTerrainSurfaces.add(new APP.BoxCenteredGeometry({
                //    center: vec3.fromValues(verticesList[cpt][0], verticesList[cpt][1] + maxYLevel * (Math.random()*randomOverride), verticesList[cpt][2] - ((finish - start) / Math.pow(2, detailLevel)) * (Math.random()*randomOverride) / 2),
                //    colorList: [vec4.fromValues(0, 1, 0, 1)],
                //    width: 0.5,
                //    height: 0.5
                //}));

            }
            //uvsList.push(vec2.fromValues(j, i));//Use texture REPEAT because uv can be above 1,1
            //indicesList.push(i * (Math.pow(2, detailLevel)+1) + j);//counter by row and column
            //normalsList.push(vec3.fromValues(0, 1, 0));//always same normal
            //groupTerrainSurfaces.add( new APP.BoxCenteredGeometry({
            //    center: verticesList[cpt],
            //    colorList: [vec4.fromValues(1, 0,1, 1)],
            //    width:0.2,
            //    height:0.2
            //}));

            //groupTerrainSurfaces.add(new APP.BoxCenteredGeometry({
            //    center: verticesList[cpt - Math.pow(2, detailLevel) - 1],
            //    colorList: [vec4.fromValues(1, 0, 0, 1)],
            //    width: 0.2,
            //    height: 0.2
            //}));
            cpt++;
            //  console.log(i + " " + " " + j + " " + cpt + " " + verticesList.length + " " + bezierZList + "\n");

        }
    }
    //console.log(verticesList, bezierXList, bezierZList);

    // TTTTTTTTTTTTTTTT    TEXTURE LOAD   TTTTTTTTTTTTTTTTTTTTTT//
    var texture = new APP.Texture({
        textureLocation: texLocation,
        textureBase64String: texString,
        isRepeating: true
        // image: image
    });
    // var texture = textureLoader.initTexture();
    // TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT//

    var cptX = 0;
    var cptZ = 0;
    //STORE ALL COONS IN THE GROUP
    for (var p = 0; p < Math.pow(2, detailLevel); p++) {
        for (var q = 0; q < Math.pow(2, detailLevel); q++) {

            coonsEquation = new APP.Math.Surfaces.CoonsSurface({

                C1: bezierXList[cptX],
                C2: bezierXList[cptX + Math.pow(2, detailLevel)],
                //    C3: bezierZList[(q * Math.pow(2, detailLevel)+p)],
                //C4: bezierZList[(q * Math.pow(2, detailLevel)+p) + Math.pow(2, detailLevel)],
                C3: bezierZList[cptZ],
                C4: bezierZList[cptZ + 1],
            });

            groupTerrainSurfaces.add(new APP.ParametricSurfaceGeometry({
                equation: coonsEquation,
                slices: slices,
                stacks: stacks,
                color: color, // Red.
                drawControlPoints: false,
                controlPointsColor: controlPointsColor, // Green.
                // strTextureImage: strTextureImage,
                texture: texture
                //        image: image  //pas au point!
            }));
            //console.log(cpt, cpt + Math.pow(2, detailLevel), cpt, cpt + 1);
            cptX++;
            cptZ++;
        }
        cptZ++;
    }
    groupTerrainSurfaces.frustumCull = false;
    // groupTerrainSurfaces.transform.position.xyz = vec3.fromValues(-30, height, 10);
    return groupTerrainSurfaces;

}

//    coonsEquation = new APP.Math.Surfaces.CoonsSurface({
//        C1: new APP.Math.Curves.BezierCubic({
//            // Control points.
//            P0: corner1,
//            P1: vec3.add(vec3.create(), corner1, vCtrPtC1_1),
//            P2: vec3.add(vec3.create(), corner2, vCtrPtC1_2),
//            P3: corner2,
//        }),
//        C2: new APP.Math.Curves.BezierCubic({
//            // Control points.
//            P0: corner3,
//            P1: vec3.add(vec3.create(), corner3, vCtrPtC2_1),
//            P2: vec3.add(vec3.create(), corner4, vCtrPtC2_2),
//            P3: corner4,
//        }),
//        C3: new APP.Math.Curves.BezierCubic({
//            // Control points.
//            P0: corner1,
//            P1: vec3.add(vec3.create(), corner1, vCtrPtC3_1),
//            P2: vec3.add(vec3.create(), corner3, vCtrPtC3_2),
//            P3: corner3,
//        }),
//        C4: new APP.Math.Curves.BezierCubic({
//            // Control points.
//            P0: corner2,
//            P1: vec3.add(vec3.create(), corner2, vCtrPtC4_1),
//            P2: vec3.add(vec3.create(), corner4, vCtrPtC4_2),
//            P3: corner4,
//        })
//    });

//    coons = new APP.ParametricSurfaceGeometry({
//        equation: coonsEquation,
//        slices: 2,
//        stacks: 2,
//        //color: vec4.fromValues(105 / 255, 180 / 255, 200 / 255, 1.0), // Red.
//        drawControlPoints: false,
//        controlPointsColor: vec4.fromValues(0.0, 1.0, 0.0, 1.0), // Green.
//        //strTexture : APP.ModelElsaOBJ.textureElsa["mesh0"]
//    });
//    groupCoons = new APP.Object3D();









///*
//    //Vertices

//    //first tile (square)
//    var v0 = vec3.fromValues(center[0] + width / 2, center[1] + height / 2, center[2]);
//    var v1 = vec3.fromValues(center[0] - width / 2, center[1] + height / 2, center[2]);
//    var v2 = vec3.fromValues(center[0] + width / 2, center[1] - height / 2, center[2]);
//    var v3 = vec3.fromValues(center[0] - width / 2, center[1] - height / 2, center[2]);




//    // UVs for texture
//    var t0 = vec2.fromValues(0, 0);
//    var t1 = vec2.fromValues(0, 1);
//    var t2 = vec2.fromValues(1, 0);
//    var t3 = vec2.fromValues(1, 1);

//    //normals (0,1,0) for every vertex
//    var normalList = [];

//    for (var k = 0; k < verticesList.length; k++) {
//        normalList.push(vec3.fromValues(0,1,0));
//    }
//*/
//    this.geometry = new APP.Geometry({
//        vertices: verticesList,
//       // colors: colorList,
//        indices: indicesList,
//        normals:normalsList,
//        uvs: uvsList

//    });

//    //this.geometry.computeNormals();
//    var gl = this.gl = APP.WebGLContext;
//    initTexture();

//    // Private drawing function.
//    this._drawParametricTerrainBuilder = function (args) {

//        if (this.isFrustumCulled(args.frustum)) {
//            // Bind uniform model matrix.
//            gl.uniformMatrix4fv(APP.ShaderProgram.uModelMatrixLocation, false, this.worldMatrix);

//            // Bind normal matrix.
//            var normalMatrix = APP.Math.computeNormalMatrix3(this.worldMatrix);
//            // Send to uniform variable.
//            gl.uniformMatrix3fv(APP.ShaderProgram.uNormalMatrixLocation, false, normalMatrix);


//            //************** SET UP DRAW TEXTURE **********************//
//            gl.activeTexture(gl.TEXTURE0);
//            gl.bindTexture(gl.TEXTURE_2D, texture);
//            gl.uniform1i(APP.ShaderProgram.samplerUniform, 0);

//            var viewMatrix = args.V;
//            var projMatrix = args.P;

//            // Bind matrices.
//            // setMatrixUniforms(viewMatrix,projMatrix);

//            var matMVP = mat4.mul(mat4.create(), viewMatrix, projMatrix); // Create MVP matrix.
//            var mInv = mat4.invert(mat4.create(), APP.ShaderProgram.uModelMatrixLocation);
//            gl.uniformMatrix4fv(mInv, false, matMVP);  // Bind MVP matrix to uniform.
//            //*********************************************************//

//            // Draw elements.
//            this.geometry.draw(gl.TRIANGLES);
//            //console.log(texture);
//            //Clean up
//            gl.bindTexture(gl.TEXTURE_2D, null);
//            //     gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_NEAREST);
//            //   gl.generateMipmap(gl.TEXTURE_2D);

//            //    gl.bindTexture(gl.TEXTURE_2D, null);

//        }
//    }

//    this.draw = function (args) {
//        this._drawParametricTerrainBuilder(args);

//        // Call draw on each children.
//        _.invoke(this.children, 'draw', args);
//    }

//    //############################  FONCTIONS POUR LES TEXTURES ##################################//



//    function handleLoadedTexture(texture) {
//        // Create a texture.
//        gl.bindTexture(gl.TEXTURE_2D, texture);
//        // gl.generateMipmap(gl.TEXTURE_2D);
//        gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, false);
//        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, texture.image);
//        // Set the parameters so we can render any size image.
//        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
//        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);
//        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
//        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);

//        gl.bindTexture(gl.TEXTURE_2D, texture);

//    }

//    var texture;
//    function initTexture() {
//        texture = gl.createTexture();
//        texture.image = new Image();
//        texture.image.onload = function () {
//            handleLoadedTexture(texture)
//        }
//        texture.image.src = strTextureImage;

//    }

//    function setMatrixUniforms(viewMatrix, projMatrix) {
//        // Model matrix
//        var matMVP = mat4.mul(mat4.create(), viewMatrix, projMatrix); // Create MVP matrix.

//        gl.uniformMatrix4fv(this.worldMatrix, false, matMVP);  // Bind MVP matrix to uniform.
//    }
//    //###############################################################################################//

//}


//APP.ParametricTerrainBuilder = Compose(APP.Object3D, APP.Events, APP.__ParametricTerrainBuilder);
