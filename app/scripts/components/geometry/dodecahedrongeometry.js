APP.__DodecahedronGeometry = function (params) {
    var defaultColor = vec4.fromValues(0.0, 0.0, 0.0, 0.0);
    var defaults = {
        center: vec3.fromValues(0, 0, 0),
        scale: 0.5,
        colorList: [defaultColor], // No color by default.
        isRepeatingTexture: false,
        textureLocation: {
            key: 'alienrock2',
            type: 'groundtexture',
            file: 'Base64GroundTexture'
        },
        texture: undefined,
        material: undefined,
        geometry: undefined
    };

    var _attrs = _.extend(defaults, _.clone(params));
    this.isRepeatingTexture = _attrs.isRepeatingTexture;
    this.textureLocation = _attrs.textureLocation;
    this.texture = _attrs.texture;
    this.material = _attrs.material;
    var geometry = _attrs.geometry;

    //Vertices
    var center = _attrs.center;
    var scale = _attrs.scale;
    var colorList = _attrs.colorList;
    var strTextureImage = _attrs.strTextureImage;


    if (_.isUndefined(geometry)) {
var v0 = vec3.fromValues( 1.21412*scale + center[0],      0*scale + center[1],          1.58931*scale + center[2]);
var v1 = vec3.fromValues( 0.375185 *scale + center[0],    1.1547*scale + center[1],     1.58931*scale + center[2]);
var v2 = vec3.fromValues( -0.982247*scale + center[0],    0.713644*scale + center[1],   1.58931*scale + center[2]);
var v3 = vec3.fromValues( -0.982247*scale + center[0],    -0.713644*scale + center[1],  1.58931*scale + center[2]);
var v4 = vec3.fromValues( 0.375185 *scale + center[0],    -1.1547*scale + center[1],    1.58931*scale + center[2]);

var v5 = vec3.fromValues( 1.96449 *scale + center[0],     0 *scale + center[1],         0.375185*scale + center[2]);
var v6 = vec3.fromValues( 0.607062*scale + center[0],     1.86835*scale + center[1],    0.375185*scale + center[2]);
var v7 = vec3.fromValues( -1.58931 *scale + center[0],    1.1547*scale + center[1],     0.375185*scale + center[2]);
var v8 = vec3.fromValues( -1.58931 *scale + center[0],    -1.1547 *scale + center[1],   0.375185*scale + center[2]);
var v9 = vec3.fromValues( 0.607062 *scale + center[0],    -1.86835 *scale + center[1],  0.375185*scale + center[2]);

var v10 = vec3.fromValues( 1.58931 *scale + center[0],     1.1547*scale + center[1],     -0.375185*scale + center[2]);
var v11 = vec3.fromValues( -0.607062*scale + center[0],    1.86835 *scale + center[1],   -0.375185*scale + center[2]);
var v12 = vec3.fromValues( -1.96449*scale + center[0],     0 *scale + center[1],         -0.375185*scale + center[2]);
var v13 = vec3.fromValues( -0.607062*scale + center[0],    -1.86835 *scale + center[1],  -0.375185*scale + center[2]);
var v14 = vec3.fromValues( 1.58931 *scale + center[0],     -1.1547*scale + center[1],    -0.375185*scale + center[2]);

var v15 = vec3.fromValues( 0.982247 *scale + center[0],    0.713644*scale + center[1],   -1.58931*scale + center[2]);
var v16 = vec3.fromValues( -0.375185 *scale + center[0],   1.1547 *scale + center[1],    -1.58931*scale + center[2]);
var v17 = vec3.fromValues( -1.21412*scale + center[0],     0*scale + center[1],          -1.58931*scale + center[2]);
var v18 = vec3.fromValues( -0.375185*scale + center[0],    -1.1547*scale + center[1],    -1.58931*scale + center[2]);
var v19 = vec3.fromValues( 0.982247 *scale + center[0],    -0.713644*scale + center[1],  -1.58931*scale + center[2]);


    var t0 = vec2.fromValues(0.35, 0.65);
    var t1 = vec2.fromValues(0.65, 0.65);
    var t2 = vec2.fromValues(0.6, 0.4);
    var t3 = vec2.fromValues(0.5, 0.2);
    var t4 = vec2.fromValues(0.3, 0.4);
    var t5 = vec2.fromValues(0.25, 0.8);
    var t6 = vec2.fromValues(0.75, 0.8);
    var t7 = vec2.fromValues(0.3, 0.875);
    var t8 = vec2.fromValues(0.5, 0);
    var t9 = vec2.fromValues(0.125, 0.3);
    var t10 = vec2.fromValues(0.5, 1.0);
    var t11 = vec2.fromValues(1.0, 0.65);
    var t12 = vec2.fromValues(0.75, 0);
    var t13 = vec2.fromValues(0.25, 0);
    var t14 = vec2.fromValues(0, 0.65);
    var t15 = vec2.fromValues(0.3, 0.6);
    var t16 = vec2.fromValues(0.5, 0.8);
    var t17 = vec2.fromValues(0.7, 0.6);
    var t18 = vec2.fromValues(0.65, 0.35);
    var t19 = vec2.fromValues(0.35, 0.35);


    this.geometry = new APP.Geometry({      
    vertices : [v0,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11,v12,v13,v14,v15,v16,v17,v18,v19],
    colors: colorList,
    indices:[
            1,0,2, 2,0,3, 3,0,4,

            5,10,0, 0,10,1, 10,1,6,
            6,11,1, 1,11,2, 2,11,7,
            7,12,2, 2,12,3, 3,12,8,
            8,13,3, 3,13,4, 4,13,9,
            9,14,4, 4,14,0, 0,14,5,
        
            14,5,19, 19,5,15, 15,5,10,
            10,6,15, 15,6,16, 16,6,11,
            11,7,16, 16,7,17, 17,7,12,
            12,8,17, 17,8,18, 18,8,13,
            13,9,18, 18,9,19, 19,9,14,

            19,15,18, 18,15,17, 17,15,16
    ],
    uvs: [t0,t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11,t12,t13,t14,t15,t16,t17,t18,t19]      
    });

    this.geometry.computeNormals();
    var gl = this.gl = APP.WebGLContext;

    if (_.isUndefined(this.material) && _.isUndefined(this.texture)) {
        // TTTTTTTTTTTTTTTT    TEXTURE LOAD   TTTTTTTTTTTTTTTTTTTTTT//
        texture = new APP.Texture({
            textureLocation: this.textureLocation,
            isRepeating: this.isRepeatingTexture
        });
        // TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT//
    }
    } else {

        // Reuse geometry.
        this.geometry = geometry;

        this.transform.position.xyz = top;
        this.transform.scale.xyz = vec3.fromValues(scale * this.transform.scale.x, scale * this.transform.scale.y, scale * this.transform.scale.z);

    }
    // Private drawing function.
    this._drawDodecahedronGeometry = function (args) {

        if (this.isFrustumCulled(args.frustum)) {

            var normalMatrix = APP.Math.computeNormalMatrix3(this.worldMatrix);

            if (_.isUndefined(this.material)) {

                var shader = args.defaultShader;
                var sampler = 0;

                // SET UP DRAW TEXTURE ********//
                texture.bind(gl.TEXTURE0, 0, shader);

                // Bind uniforms.
                shader.uniforms['ModelMatrix'].value = this.worldMatrix;
                // shader.uniforms['UseTextures'].value = true; // Use color
                shader.uniforms['UseLighting'].value = true; // Use lighting
                shader.uniforms['NormalMatrix'].value = normalMatrix;
                // shader.uniforms['texture_diffuse'].value = 0;

                //*********************************************************//

                // Draw elements.
                this.geometry.draw(gl.TRIANGLES);

                //Clean up
                texture.unbind();

                if (_attrs.drawControlPoints) {
                    self._controlPointsGeometry.draw(gl.LINE_STRIP);
                }

            } else {

                this.material.bind({
                    'ModelMatrix': this.worldMatrix,
                    'ViewMatrix': args.viewMatrix,
                    'ProjMatrix': args.projMatrix,
                    'UseLighting': true,
                    'NormalMatrix': normalMatrix,
                    'd_ambient': args.ambientLightColor,
                    'd_diffuse': args.directionalLight.color,
                    'd_specular': args.directionalLight.specular,
                    'd_direction': args.directionalLight.direction
                });


                // Draw elements.
                this.geometry.draw(gl.TRIANGLES);

                this.material.unbind(args.defaultShader);

            }
        }
    }

    this.draw = function (args) {
        this._drawDodecahedronGeometry(args);

        // Call draw on each children.
        _.invoke(this.children, 'draw', args);
    }
}


APP.DodecahedronGeometry = Compose(APP.Object3D, APP.Events, APP.__DodecahedronGeometry);










