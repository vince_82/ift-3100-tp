APP.__ParametricSurfaceGeometry = function(params) {

    var defaults = {
        // Control points.
        equation: new APP.Math.Surfaces.CoonsSurface(),
        slices: 20,
        stacks: 20,
        color: vec4.fromValues(0.0, 0.0, 0.0, 0.0), // Nocolor by default.
        drawControlPoints: false,
        controlPointsColor: vec4.fromValues(0.6, 0.6, 0.6, 1.0), // Grey by default.
        // strTextureImage: APP.Base64GroundTexture.groundtexture["grass"],
        // image: new Image(),
        texture: undefined,
        material: undefined
            // frustumCulling:false
    };
    //    var frustumCulling = _attrs.frustumCulling;
    ////Amaury: no cull for terrain
    //    if (frustumCulling == true) {
    //    this.frustumCull = true;
    //}else{this.frustumCull = false;}
    this.frustumCull = false;


    var _attrs = _.extend(defaults, _.clone(params));
    var _curves = _.pick(_attrs.equation, ['C1', 'C2', 'C3', 'C4']);
    var strTextureImage = _attrs.strTextureImage;
    // var image = _attrs.image;
    var texture = _attrs.texture;

    this.material = _attrs.material;

    //   console.log(strTextureImage);
    var _controlPoints = _.chain(_.pick(_curves, ['C1', 'C4', 'C2', 'C3']))
        .map(function(c, i) {

            switch (i) {
                case 'C1':
                    return _.values(c.controlPoints);
                    break;
                case 'C2':
                    return _.values(c.controlPoints).reverse();
                    break;
                case 'C3':
                    return _.values(c.controlPoints).reverse();
                    break;
                case 'C4':
                    return _.values(c.controlPoints);
                    break;
            }

        })
        .flatten()
        .value();

    var gl = this.gl = APP.WebGLContext;

    var self = this;

    _.each(['equation', 'slices', 'stacks', 'C1', 'C2', 'C3', 'C4'], function(prop) {
        Object.defineProperty(self, prop, {
            get: function() {
                return _attrs[prop];
            },
            set: function(value) {
                _attrs[prop] = value;
                this.trigger('change');
            }
        });
    });

    Object.defineProperty(this, 'drawControlPoints', {
        get: function() {
            return _attrs.drawControlPoints;
        },
        set: function(value) {
            _attrs.drawControlPoints = value;
        }
    });

    var sample = this.equation.getPoint;

    this.sampleSurface = function() {
        var vertices = [],
            indices = [],
            uvs = [];
        var deltaU = 1.0 / self.slices,
            deltaV = 1.0 / self.stacks;

        var i, j, u, v;

        var sliceCount = self.slices + 1;

        for (i = 0; i <= self.stacks; ++i) {

            for (j = 0; j <= self.slices; ++j) {

                var s = sample(j * deltaU, i * deltaV);
                vertices.push(s);

                // console.log('Sampling for (' + j * deltaU + ', ' + i * deltaV + ')');
                // console.log(s);
            }

        }

        for (i = 0; i < self.stacks; i++) {

            for (j = 0; j < self.slices; j++) {

                // ============================================
                // Source : Three.js -> ParametricGeometry.js
                // https://github.com/mrdoob/three.js/blob/master/src/extras/geometries/ParametricGeometry.js

                var a = (i * sliceCount) + j;
                var b = (i * sliceCount) + j + 1;
                var c = ((i + 1) * sliceCount) + j + 1;
                var d = ((i + 1) * sliceCount) + j;

                var uva = vec2.fromValues(j * deltaU, 1 - i * deltaV);
                var uvb = vec2.fromValues((j + 1) * deltaU, 1 - i * deltaV);
                var uvc = vec2.fromValues((j + 1) * deltaU, 1 - (i + 1) * deltaV);
                var uvd = vec2.fromValues(j * deltaU, 1 - (i + 1) * deltaV);

                // ============================================

                indices.push([a, b, d, b, c, d]);
                // uvs.push([uva, uvb, uvd, uvb, uvc, uvd]);


            }

        }
        //TEST UV///////////////
        for (var i = 0; i < vertices.length; i++) {
            uvs.push(vec2.fromValues(vertices[i][0], vertices[i][2]));
        }
        // console.log(indices, uvs, vertices);


        ///////////////////////


        indices = _.flatten(indices);
        uvs = _.flatten(uvs);

        // Change the shader to the material's shader if it exists.
        // It's important for the geometry.
        var shader = (_.isUndefined(self.material)) ? APP.Shaders.defaultShader : self.material.shader;

        if (!_.isUndefined(self.geometry)) {
            self.geometry.shader = shader;
            self.geometry.vertices = vertices;
            self.geometry.indices = indices;
            self.geometry.uvs = uvs;
        } else {
            self.geometry = new APP.Geometry({
                vertices: vertices,
                indices: indices,
                uvs: uvs,
                colors: [_attrs.color],
                shader: shader
            });
        }

        self.geometry.computeNormals();

        if (!_.isUndefined(self._controlPointsGeometry)) {
            self._controlPointsGeometry.vertices = _controlPoints;
        } else {
            self._controlPointsGeometry = new APP.Geometry({
                vertices: _controlPoints,
                colors: [_attrs.controlPointsColor]
            });
        }

    }

    if (_.isUndefined(this.material) && _.isUndefined(texture)) {
        // TTTTTTTTTTTTTTTT    TEXTURE LOAD   TTTTTTTTTTTTTTTTTTTTTT//
        texture = new APP.Texture({
            strTextureImage: strTextureImage,
            isRepeating: true
        });
        // texture = textureLoader.initTexture();
        // TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT//
    }


    // Private drawing function.
    this._drawSurface = function(args) {

        // Bind uniform model matrix.
        if (this.isFrustumCulled(args.frustum)) {


            var normalMatrix = APP.Math.computeNormalMatrix3(this.worldMatrix);

            if (_.isUndefined(this.material)) {

                var shader = args.defaultShader;
                var sampler = 0;

                //************** SET UP DRAW TEXTURE **********************//
                texture.bind(gl.TEXTURE0, 0, shader);

                // Bind uniforms.
                shader.uniforms['ModelMatrix'].value = this.worldMatrix;
                // shader.uniforms['UseTextures'].value = true; // Use color
                shader.uniforms['UseLighting'].value = true; // Use lighting
                shader.uniforms['NormalMatrix'].value = normalMatrix;
                // shader.uniforms['texture_diffuse'].value = 0;

                //*********************************************************//

                // Draw elements.
                this.geometry.draw(gl.TRIANGLES);

                //Clean up
                texture.unbind();

                if (_attrs.drawControlPoints) {
                    self._controlPointsGeometry.draw(gl.LINE_STRIP);
                }

            } else {

                this.material.bind({
                    'ModelMatrix': this.worldMatrix,
                    'ViewMatrix': args.viewMatrix,
                    'ProjMatrix': args.projMatrix,
                    'UseLighting': true,
                    'NormalMatrix': normalMatrix,
                    'd_ambient': args.ambientLightColor,
                    'd_diffuse': args.directionalLight.color,
                    'd_specular': args.directionalLight.specular,
                    'd_direction': args.directionalLight.direction
                });


                // Draw elements.
                this.geometry.draw(gl.TRIANGLES);

                this.material.unbind(args.defaultShader);

            }
        }

    }

    this.draw = function(args) {
        this._drawSurface(args);

        // Call draw on each children.
        _.invoke(this.children, 'draw', args);
    }

}


APP.ParametricSurfaceGeometry = Compose(APP.Object3D, APP.Events, APP.__ParametricSurfaceGeometry, function() {
    this.listenTo(this, 'change', this.sampleSurface);
    this.sampleSurface();
});
