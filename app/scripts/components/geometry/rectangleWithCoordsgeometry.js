// Inherit from Object3D.
// this.attributes contains :
// - pt1, pt2, pt3 and pt4 -> points defining the Rectrangle.
// - color/texture.
// Pass an object to the constructor containing those attributes.
// They will be copied to 'this.attributes'


APP.__RectangleWithCoordsGeometry = function(params) {
    var defaultColor = vec4.fromValues(0.0, 0.0, 0.0, 0.0);
    var defaults = {
        //Control points.
        v0: vec3.fromValues(0, 0, 0),
        v1: vec3.fromValues(1, 0, 0),
        v2: vec3.fromValues(0, 1, 0),
        v3: vec3.fromValues(1, 1, 0),
        colorList: [defaultColor], // No color by default.
        // strTextureImage: APP.Base64GroundTexture.groundtexture["snow"],
        isInXYplane: false,
        isInYZplane: false,
        isInXZplane: false,
        isRepeatingTexture: false,
        textureLocation: {
            key: 'energyrock',
            type: 'groundtexture',
            file: 'Base64GroundTexture'
        },
        texture: undefined,
        material: undefined,
        geometry: undefined
    };

    var _attrs = _.extend(defaults, _.clone(params));
    this.isRepeatingTexture = _attrs.isRepeatingTexture;
    this.textureLocation = _attrs.textureLocation;
    var texture = _attrs.texture;
    this.material = _attrs.material;
    var geometry = _attrs.geometry;

    //Vertices
    var v0 = _attrs.v0;
    var v1 = _attrs.v1;
    var v2 = _attrs.v2;
    var v3 = _attrs.v3;
    var colorList = _attrs.colorList;
    var strTextureImage = _attrs.strTextureImage;
    var isInXYplane = _attrs.isInXYplane;
    var isInYZplane = _attrs.isInYZplane;
    var isInXZplane = _attrs.isInXZplane;


    //UV
    if (this.isRepeatingTexture == true) {


        if (isInXYplane == true) {
            var t0 = vec2.fromValues(v0[0], v0[1]);
            var t1 = vec2.fromValues(v1[0], v1[1]);
            var t2 = vec2.fromValues(v2[0], v2[1]);
            var t3 = vec2.fromValues(v3[0], v3[1]);
        } else if (isInYZplane == true) {
            var t0 = vec2.fromValues(v0[1], v0[2]);
            var t1 = vec2.fromValues(v1[1], v1[2]);
            var t2 = vec2.fromValues(v2[1], v2[2]);
            var t3 = vec2.fromValues(v3[1], v3[2]);
        } else if (isInXZplane == true) {
            var t0 = vec2.fromValues(v0[0], v0[2]);
            var t1 = vec2.fromValues(v1[0], v1[2]);
            var t2 = vec2.fromValues(v2[0], v2[2]);
            var t3 = vec2.fromValues(v3[0], v3[2]);
        }
    } else {
        var t0 = vec2.fromValues(0, 0);
        var t1 = vec2.fromValues(0, 1);
        var t2 = vec2.fromValues(1, 0);
        var t3 = vec2.fromValues(1, 1);
    }


    this.geometry = new APP.Geometry({
        vertices: [v0, v1, v2, v3],
        colors: colorList,
        indices: [0, 1, 2, 2, 1, 3],
        normals: [vec3.fromValues(0, 1, 0),
            vec3.fromValues(0, 1, 0),
            vec3.fromValues(0, 1, 0),
            vec3.fromValues(0, 1, 0)
        ],
        uvs: [t2,
            t0,
            t3,
            t1,
        ]

    });

    this.geometry.computeNormals();

    var gl = this.gl = APP.WebGLContext;

    if (_.isUndefined(this.material) && _.isUndefined(texture)) {
        // TTTTTTTTTTTTTTTT    TEXTURE LOAD   TTTTTTTTTTTTTTTTTTTTTT//
        texture = new APP.Texture({
            textureLocation: this.textureLocation,
            isRepeating: this.isRepeatingTexture
        });
        // TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT//
    }

    // Private drawing function.
    this._drawRectangleWithCoordsGeometry = function(args) {

        if (this.isFrustumCulled(args.frustum)) {


            var normalMatrix = APP.Math.computeNormalMatrix3(this.worldMatrix);

            if (_.isUndefined(this.material)) {

                var shader = args.defaultShader;
                var sampler = 0;

                //************** SET UP DRAW TEXTURE **********************//
                texture.bind(gl.TEXTURE0, 0, shader);

                // Bind uniforms.
                shader.uniforms['ModelMatrix'].value = this.worldMatrix;
                // shader.uniforms['UseTextures'].value = true; // Use color
                shader.uniforms['UseLighting'].value = true; // Use lighting
                shader.uniforms['NormalMatrix'].value = normalMatrix;
                // shader.uniforms['texture_diffuse'].value = 0;

                //*********************************************************//

                // Draw elements.
                this.geometry.draw(gl.TRIANGLES);

                //Clean up
                texture.unbind();

                if (_attrs.drawControlPoints) {
                    self._controlPointsGeometry.draw(gl.LINE_STRIP);
                }

            } else {

                this.material.bind({
                    'ModelMatrix': this.worldMatrix,
                    'ViewMatrix': args.viewMatrix,
                    'ProjMatrix': args.projMatrix,
                    'UseLighting': true,
                    'NormalMatrix': normalMatrix,
                    'd_ambient': args.ambientLightColor,
                    'd_diffuse': args.directionalLight.color,
                    'd_specular': args.directionalLight.specular,
                    'd_direction': args.directionalLight.direction
                });


                // Draw elements.
                this.geometry.draw(gl.TRIANGLES);

                this.material.unbind(args.defaultShader);

            }


        }
    }

    this.draw = function(args) {
        this._drawRectangleWithCoordsGeometry(args);

        // Call draw on each children.
        _.invoke(this.children, 'draw', args);
    }
}


APP.RectangleWithCoordsGeometry = Compose(APP.Object3D, APP.Events, APP.__RectangleWithCoordsGeometry);
