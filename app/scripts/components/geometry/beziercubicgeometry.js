APP.__BezierCubicGeometry = function(params) {

    var defaults = {
        // Control points.
        equation: new APP.Math.Curves.BezierCubic(),
        samples: 20,
        color: vec4.fromValues(0.0, 0.0, 0.0, 1.0), // Black by default.
        drawControlPoints: false,
        controlPointsColor: vec4.fromValues(0.6, 0.6, 0.6, 1.0), // Grey by default.
    };

    var _attrs = _.extend(defaults, _.clone(params));
    var _controlPoints = _.pick(_attrs.equation, ['P0', 'P1', 'P2', 'P3']);

    var gl = this.gl = APP.WebGLContext;

    var self = this;

    _.each(['samples', 'equation'], function(prop) {
        Object.defineProperty(self, prop, {
            get: function() {
                return _attrs[prop];
            },
            set: function(value) {
                _attrs[prop] = value;
                this.trigger('change');
            }
        });
    });

    _.each(['P0', 'P1', 'P2', 'P3'], function(prop) {
        Object.defineProperty(self, prop, {
            get: function() {
                return _attrs.equation[prop];
            },
            set: function(value) {
                _attrs.equation[prop] = value;
                this.trigger('change');
            }
        });
    });

    Object.defineProperty(this, 'drawControlPoints', {
        get: function() {
            return _attrs.drawControlPoints;
        },
        set: function(value) {
            _attrs.drawControlPoints = value;
        }
    });

    this.sampleCurve = function() {
        var vertices = [];
        var delta = 1.0 / self.samples;

        for (var i = 0; i <= _attrs.samples; ++i) {
            vertices.push(self.equation.getPoint(i * delta));
        }

        if (!_.isUndefined(self.geometry)) {
            self.geometry.vertices = vertices;
        } else {
            self.geometry = new APP.Geometry({
                vertices: vertices,
                colors: [_attrs.color]
            });
        }

        if (!_.isUndefined(self._controlPointsGeometry)) {
            self._controlPointsGeometry.vertices = _.values(_controlPoints);
        } else {
            self._controlPointsGeometry = new APP.Geometry({
                vertices: _.values(_controlPoints),
                colors: [_attrs.controlPointsColor]
            });
        }
    }

    // Private drawing function.
    this._drawCurve = function(args) {

        var shader = args.defaultShader;

        // Bind uniform model matrix.
        shader.uniforms['ModelMatrix'].value = this.worldMatrix;
        shader.uniforms['UseTextures'].value = false; // Use color
        shader.uniforms['UseLighting'].value = false; // No lighting

        // Draw elements.
        this.geometry.draw(gl.LINE_STRIP);

        if (_attrs.drawControlPoints) {
            self._controlPointsGeometry.draw(gl.LINE_STRIP);
        }
    }

    this.draw = function(args) {
        this._drawCurve(args);

        // Call draw on each children.
        _.invoke(this.children, 'draw');
    }

};

APP.BezierCubicGeometry = Compose(APP.Object3D, APP.Events, APP.__BezierCubicGeometry, function() {
    this.listenTo(this, 'change', this.sampleCurve);
    this.sampleCurve();
});
