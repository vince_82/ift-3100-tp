APP.__OctahedronGeometry = function(params) {
    var defaultColor = vec4.fromValues(0.0, 0.0, 0.0, 0.0);
    var defaults = {
        center: vec3.fromValues(0, 0, 0),
        width: 1,
        height: 1,
        depth: 1,
        colorList: [defaultColor], // No color by default.
        isRepeatingTexture: false,
        textureLocation: {
            key: 'alienrock1',
            type: 'groundtexture',
            file: 'Base64GroundTexture'
        },
        texture: undefined,
        material: undefined,
        geometry: undefined
    };

    var _attrs = _.extend(defaults, _.clone(params));
    this.isRepeatingTexture = _attrs.isRepeatingTexture;
    this.textureLocation = _attrs.textureLocation;
    var texture = _attrs.texture;
    this.material = _attrs.material;
    var geometry = _attrs.geometry;

    if (_.isUndefined(geometry)) {
    //Vertices
    var center = _attrs.center;
    var height = _attrs.height;
    var width = _attrs.width;
    var depth = _attrs.depth;
    var colorList = _attrs.colorList;
    var strTextureImage = _attrs.strTextureImage;

    var v0 = vec3.fromValues(center[0], center[1] + height, center[2]);
    var v1 = vec3.fromValues(center[0] - width / 2, center[1], center[2] - depth / 2);
    var v2 = vec3.fromValues(center[0] - width / 2, center[1], center[2] + depth / 2);
    var v3 = vec3.fromValues(center[0] + width / 2, center[1], center[2] - depth / 2);
    var v4 = vec3.fromValues(center[0] + width / 2, center[1], center[2] + depth / 2);
    var v5 = vec3.fromValues(center[0], center[1] - height, center[2]);

    var t0 = vec2.fromValues(0.5, 0.5);
    var t1 = vec2.fromValues(1, 0);
    var t2 = vec2.fromValues(1, 1);
    var t3 = vec2.fromValues(0, 0);
    var t4 = vec2.fromValues(0, 1);
    var t5 = vec2.fromValues(0.5, 0.5);

    var uvList = [];
    for (var i = 0; i < 4; i++) {
        uvList.push(t0);
        uvList.push(t1);
        uvList.push(t2);
    }

    this.geometry = new APP.Geometry({

        vertices: [v0, v1, v2, v3, v4, v5],
        colors: colorList,
        indices: [
            1, 0, 2, 2, 0, 4, 4, 0, 3, 3, 0, 1,
            1, 5, 2, 2, 5, 4, 4, 5, 3, 3, 5, 1
        ],
        uvs: [t0, t1, t2, t3, t4, t5]
    });

    this.geometry.computeNormals();
    var gl = this.gl = APP.WebGLContext;

    if (_.isUndefined(this.material) && _.isUndefined(texture)) {
        // TTTTTTTTTTTTTTTT    TEXTURE LOAD   TTTTTTTTTTTTTTTTTTTTTT//
        texture = new APP.Texture({
            textureLocation: this.textureLocation,
            isRepeating: this.isRepeatingTexture
        });
        // TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT//
    }
    } else {

        // Reuse geometry.
        this.geometry = geometry;

        this.transform.position.xyz = center;
        this.transform.scale.xyz = vec3.fromValues(width, height, depth);

    }
    // Private drawing function.
    this._drawOctahedronGeometry = function(args) {

        if (this.isFrustumCulled(args.frustum)) {


            var normalMatrix = APP.Math.computeNormalMatrix3(this.worldMatrix);

            if (_.isUndefined(this.material)) {

                var shader = args.defaultShader;
                var sampler = 0;

                //************** SET UP DRAW TEXTURE **********************//
                texture.bind(gl.TEXTURE0, 0, shader);

                // Bind uniforms.
                shader.uniforms['ModelMatrix'].value = this.worldMatrix;
                // shader.uniforms['UseTextures'].value = true; // Use color
                shader.uniforms['UseLighting'].value = true; // Use lighting
                shader.uniforms['NormalMatrix'].value = normalMatrix;
                // shader.uniforms['texture_diffuse'].value = 0;

                //*********************************************************//

                // Draw elements.
                this.geometry.draw(gl.TRIANGLES);

                //Clean up
                texture.unbind();

                if (_attrs.drawControlPoints) {
                    self._controlPointsGeometry.draw(gl.LINE_STRIP);
                }

            } else {

                this.material.bind({
                    'ModelMatrix': this.worldMatrix,
                    'ViewMatrix': args.viewMatrix,
                    'ProjMatrix': args.projMatrix,
                    'UseLighting': true,
                    'NormalMatrix': normalMatrix,
                    'd_ambient': args.ambientLightColor,
                    'd_diffuse': args.directionalLight.color,
                    'd_specular': args.directionalLight.specular,
                    'd_direction': args.directionalLight.direction
                });


                // Draw elements.
                this.geometry.draw(gl.TRIANGLES);

                this.material.unbind(args.defaultShader);

            }
        }
    }

    this.draw = function(args) {
        this._drawOctahedronGeometry(args);

        // Call draw on each children.
        _.invoke(this.children, 'draw', args);
    }
}


APP.OctahedronGeometry = Compose(APP.Object3D, APP.Events, APP.__OctahedronGeometry);
