// Inherit from Object3D.
// this.attributes contains :
// - pt1 -> point that will be draw.
// - size of point.
// Pass an object to the constructor containing those attributes.
// They will be copied to 'this.attributes'
APP.__Point = function() {

    // Initialization. This function is executed along with the ctor function.

    // Private drawing function.
    this._drawPoint = function() {
        // Select vao as active object.  
        this.ext.bindVertexArrayOES(this.vao);
        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.vertexBuffer);

        var shader = args.defaultShader;

        // Bind uniform model matrix.
        shader.uniforms['ModelMatrix'].value = this.worldMatrix;
        shader.uniforms['UseTextures'].value = false; // Use color
        shader.uniforms['UseLighting'].value = false; // Use lighting
        
        // Draw the point.
        this.gl.drawArrays(this.gl.POINTS, 0, 1);

        // Unbind vao.  
        this.ext.bindVertexArrayOES(null);
    }

    this.draw = function(args) {
        this._drawPoint(args);

        // Call draw on each children.
        _.invoke(this.children, 'draw', args);
    }

}
APP.Point = Compose(APP.Object3D, APP.__Point, function() {

    // Initialization. This function is executed along with the ctor function.

    this.gl = APP.WebGLContext;
    var positionAttrib = APP.ShaderProgram.vertexPositionAttribute;

    // Get the Vertex Array Object extension and create/bind a VAO  
    this.ext = this.gl.getExtension("OES_vertex_array_object");
    this.vao = this.ext.createVertexArrayOES();

    // Select vao as active object.  
    this.ext.bindVertexArrayOES(this.vao);

    // Create buffers. 
    this.vertexBuffer = this.gl.createBuffer();
    this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.vertexBuffer);
    // Get the 3 coordinates of the point inside one array.
    var vertices = this.attributes.pt1;
    this.gl.bufferData(this.gl.ARRAY_BUFFER, vertices, this.gl.STATIC_DRAW);

    // Set pointer to vertex attribute.
    this.gl.vertexAttribPointer(positionAttrib, 3, this.gl.FLOAT, false, 0, 0);
    this.gl.enableVertexAttribArray(positionAttrib);

    // Unbind vao.  
    this.ext.bindVertexArrayOES(null);


});
