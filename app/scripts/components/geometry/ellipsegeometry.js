// Inherits from Object3D.
// this.attributes contains :
// - center -> point defining the center of the ellipse.
// - triangleAmount -> precision of the ellipse
// - width,height,
// - color.
// Pass an object to the constructor containing those attributes.
// They will be copied to 'this.attributes'

APP.__EllipseGeometry = function(params) {
    var defaultColor = vec4.fromValues(0.0, 0.0, 0.0, 0.0);
    var defaults = {
        center: vec3.fromValues(0, 0, 0),
        width: 1,
        height: 1,
        triangleAmount: 50,
        colorList: [defaultColor], // No color by default.
        isRepeatingTexture: false,
        textureLocation: {
            key: 'water',
            type: 'groundtexture',
            file: 'Base64GroundTexture'
        },
        texture: undefined,
        material: undefined,
        geometry: undefined
    };

    var _attrs = _.extend(defaults, _.clone(params));
    this.isRepeatingTexture = _attrs.isRepeatingTexture;
    this.textureLocation = _attrs.textureLocation;
    var texture = _attrs.texture;
    this.material = _attrs.material;
    var geometry = _attrs.geometry;


    //Init parameters
    var center = _attrs.center;
    var height = _attrs.height;
    var width = _attrs.width;
    var triangleAmount = _attrs.triangleAmount;
    var colorList = _attrs.colorList;


    if (_.isUndefined(geometry)) {
    //Vertices
    var uvnext, ptnext;
    var uvList = [];
    var vertlist = [];
    var indiceslist = [];
    var normalList = [];
    for (i = 0; i < triangleAmount; i++) {

        ptnext = vec3.fromValues(center[0] + (width * Math.cos(i * Math.PI * 2 / triangleAmount)),
            center[1] + (height * Math.sin(i * Math.PI * 2 / triangleAmount)), center[2]);
        vertlist[i] = ptnext;
        indiceslist[i] = i;
        uvnext = vec2.fromValues(0.5 + Math.cos(i * Math.PI * 2 / triangleAmount), 0.5 + Math.sin(i * Math.PI * 2 / triangleAmount));
        vertlist[i] = ptnext;
        uvList[i] = uvnext;
        normalList.push(vec3.fromValues(-1, 0, 0));
        //test avec rouge d�grad� (enlever la ligne suivante pour faire une ellipse normale)
        //colorList[i] = vec4.fromValues(colorList[i][0]-0.04*i, colorList[i][1], colorList[i][2], colorList[i][3]);
    }

    this.geometry = new APP.Geometry({
        vertices: vertlist, //[pt1, pt2, pt3, pt4],
        colors: colorList, //color ? [color, color, color, color] : [],
        indices: indiceslist, //[0, 1, 2, 2, 1, 3]
        normals: normalList,
        uvs: uvList

    });


    /// this.geometry.computeNormals();
    var gl = this.gl = APP.WebGLContext;

    if (_.isUndefined(this.material) && _.isUndefined(texture)) {
        // TTTTTTTTTTTTTTTT    TEXTURE LOAD   TTTTTTTTTTTTTTTTTTTTTT//
        texture = new APP.Texture({
            textureLocation: this.textureLocation,
            isRepeating: this.isRepeatingTexture
        });
        // TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT//
    }
    } else {

        // Reuse geometry.
        this.geometry = geometry;

        this.transform.position.xyz = center;
        this.transform.scale.xyz = vec3.fromValues(width, height, 0);

    }
    // Private drawing function.
    this.__EllipseGeometry = function(args) {

        if (this.isFrustumCulled(args.frustum)) {



            var normalMatrix = APP.Math.computeNormalMatrix3(this.worldMatrix);

            if (_.isUndefined(this.material)) {

                var shader = args.defaultShader;
                var sampler = 0;

                //************** SET UP DRAW TEXTURE **********************//
                texture.bind(gl.TEXTURE0, 0, shader);

                // Bind uniforms.
                shader.uniforms['ModelMatrix'].value = this.worldMatrix;
               //  shader.uniforms['UseTextures'].value = true; // Use color
                shader.uniforms['UseLighting'].value = true; // Use lighting
                shader.uniforms['NormalMatrix'].value = normalMatrix;
                // shader.uniforms['texture_diffuse'].value = 0;

                //*********************************************************//

                // Draw elements.
                this.geometry.draw(gl.TRIANGLES);

                //Clean up
                texture.unbind();

                if (_attrs.drawControlPoints) {
                    self._controlPointsGeometry.draw(gl.LINE_STRIP);
                }

            } else {

                this.material.bind({
                    'ModelMatrix': this.worldMatrix,
                    'ViewMatrix': args.viewMatrix,
                    'ProjMatrix': args.projMatrix,
                    'UseLighting': true,
                    'NormalMatrix': normalMatrix,
                    'd_ambient': args.ambientLightColor,
                    'd_diffuse': args.directionalLight.color,
                    'd_specular': args.directionalLight.specular,
                    'd_direction': args.directionalLight.direction
                });


                // Draw elements.
                this.geometry.draw(gl.TRIANGLES);

                this.material.unbind(args.defaultShader);

            }
        }
    }

    this.draw = function(args) {
        this.__EllipseGeometry(args);

        // Call draw on each children.
        _.invoke(this.children, 'draw', args);
    }
}


APP.EllipseGeometry = Compose(APP.Object3D, APP.Events, APP.__EllipseGeometry);
