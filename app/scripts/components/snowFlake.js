// SnowFlake
//Create snow flake L-System
APP.__SnowFlake = function (params) {
    var gl = this.gl = APP.WebGLContext;
    var defaultColor = vec4.fromValues(1.0, 1.0, 1.0, 1.0);
    var defaults = {
        center:vec3.fromValues(0,0,0),//Position
        color: [defaultColor], // White by default.
        detaillevel: 2,//amount of iterations (0 is a hollow triangle)
        scaleX: 1,
        scaleY: 1,
        scaleZ: 1,
    };

    var _attrs = _.extend(defaults, _.clone(params));

    //Parameters
    var center = _attrs.center;
    var color = _attrs.color;
    var detaillevel = _attrs.detaillevel;
    var scaleX = _attrs.scaleX;
    var scaleY = _attrs.scaleY;
    var scaleZ = _attrs.scaleZ;


    //Var for drawing
    var geometry;
    vertices = [];
    indices = [];

    var bottomA = vec2.fromValues(center[0] - scaleX / 2, center[1] - scaleY / 2);
    var bottomB = vec2.fromValues(center[0] + scaleX / 2, center[1] - scaleY / 2);
    var rightA = vec2.fromValues(center[0], center[1] + scaleY / 2);
    var rightB = vec2.fromValues(center[0] + scaleX / 2, center[1] - scaleY / 2);
    var leftA = vec2.fromValues(center[0] - scaleX / 2, center[1] - scaleY / 2);
    var leftB = vec2.fromValues(center[0], center[1] + scaleY / 2);



                                                      // \\
    fractal(rightB, rightA, detaillevel);  // left   //   \\   right
                                                    //     \\
    fractal(leftB, leftA, detaillevel);            //       \\
                                                  //         \\
    fractal(bottomA, bottomB, detaillevel);      // ========= \\
    //Generate line                                  bottom        


    // functions
    // source : http://www.cs.utoronto.ca/~noam/fractal.html
    function fractal(A, B, detaillevel) {

        if (detaillevel < 0) {
            return null;
        }
        
        var C = divide(add(multiply(A, 2), B), 3);
        var D = divide(add(multiply(B, 2), A), 3);
        var F = divide(add(A, B), 2);

        var V1 = divide(minus(F, A), length(F, A));
        var V2 = vec2.fromValues(V1[1], -V1[0]);

        var E = add(multiply(V2, Math.sqrt(3) / 6 * length(B, A)), F);
        
        //DrawLine(A, B, "black");

        vertices.push(vec3.fromValues(A[0],A[1],center[2]));
       // vertices.push(vec3.fromValues(B[0], B[1], center[2]));



        //if (detaillevel != 0) {
        //    for (var i = 0; i < 10; i++)
        //         DrawLine(C, D, "white");
        //        verticesErase.push(vec3.fromValues(C[0],C[1], center[2]));
        //        verticesErase.push(vec3.fromValues(D[0], D[1], center[2]));
        //};
        fractal(A, C, detaillevel - 1);
        fractal(C, E, detaillevel - 1);
        fractal(E, D, detaillevel - 1);
        fractal(D, B, detaillevel - 1);

    };

    function multiply(v, num) {
        return vec2.fromValues(v[0] * num, v[1] * num);
    };

    function divide(v, num) {
        return vec2.fromValues(v[0] / num, v[1] / num);
    };

    function add(a, b) {
        return vec2.fromValues(a[0] + b[0], a[1] + b[1]);
    };

    function minus(a, b) {
        return vec2.fromValues(a[0] - b[0], a[1] - b[1]);
    };

    function length(a, b) {
        return Math.sqrt(   Math.pow(a[0] - b[0], 2) +
                            Math.pow(a[1] - b[1], 2));
    };
    
    var verticesUnique = [];
    //Clean duplicates
    verticesUnique = _.uniq(vertices, false, function (item) {

        return [item[0], item[1],item[2]];
    });


    //Indices
    for (var j = 0; j < vertices.length; j++) {

        indices.push(j);
    }


    geometry = new APP.Geometry({
        vertices: verticesUnique,
        colors: color,
       // indices: indices,
        // normals: orderedNormalList,
        // uvs: orderedTextureList,
        //  drawMode: this.gl.TRIANGLES,
        //  boundingSphere: new APP.Math.Sphere(),
        //  shader: APP.Shaders.defaultShader
    });


    // Private drawing function.
    this._drawSnowFlake = function(args) {


        if (this.isFrustumCulled(args.frustum)) {

            var shader = args.defaultShader;

            // Bind uniforms.
            shader.uniforms.setValues({
                'ModelMatrix': this.worldMatrix,
                'NormalMatrix': APP.Math.computeNormalMatrix3(this.worldMatrix),
                'UseTextures': false,
                'UseLighting': false,
                'Sampler': 0
            })

            // Draw elements.
            geometry.draw(gl.LINE_LOOP);

        }

    }

    this.draw = function(args) {
        this._drawSnowFlake(args);

        // Call draw on each children.
        _.invoke(this.children, 'draw', args);

    }

}

APP.SnowFlake = Compose(APP.Object3D, APP.Events, APP.__SnowFlake);
