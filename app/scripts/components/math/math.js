APP.Math.composeMatrices = function(quat, scale, position) {
    var _scale = (scale.getFloat32Array) ? scale.getFloat32Array() : scale;
    var _position = (position.getFloat32Array) ? position.getFloat32Array() : position;

    // Get rotation matrix from quaternion.
    var rotation = mat4.fromQuat(mat4.create(), quat);
    // Apply scaling to rotation.
    var rs = mat4.scale(mat4.create(), rotation, _scale);

    // Apply position and return the result.

    return APP.Math.setPosition(rs, _position);
}

APP.Math.setPosition = function(matrix4, position) {
    var _position = (position.getFloat32Array) ? position.getFloat32Array() : position;
    matrix4[12] = _position[0];
    matrix4[13] = _position[1];
    matrix4[14] = _position[2];
    return matrix4;
}

APP.Math.computeNormalMatrix4 = function(matrix) {
    var normalMatrix = mat4.create();
    // The normal matrix is the inverse transposed 
    // of the transformation matrix.
    var mInv = mat4.invert(mat4.create(), matrix);
    return mat4.transpose(mat4.create(), mInv);
}

APP.Math.computeNormalMatrix3 = function(matrix) {
    return mat3.fromMat4(mat3.create(), APP.Math.computeNormalMatrix4(matrix));
}

APP.Math.makeQuatFromEuler = function(euler) {
    var _euler = (euler.getFloat32Array) ? euler.getFloat32Array() : euler;

    var out = quat.create();

    var pitch = _euler[0] / 180 * Math.PI;
    var yaw = _euler[1] / 180 * Math.PI;
    var roll = _euler[2] / 180 * Math.PI;

    var c1 = Math.cos(yaw / 2);
    var s1 = Math.sin(yaw / 2);
    var c2 = Math.cos(roll / 2);
    var s2 = Math.sin(roll / 2);
    var c3 = Math.cos(pitch / 2);
    var s3 = Math.sin(pitch / 2);
    var c1c2 = c1 * c2;
    var s1s2 = s1 * s2;
    out[0] = c1c2 * s3 + s1s2 * c3;
    out[1] = s1 * c2 * c3 + c1 * s2 * s3;
    out[2] = c1 * s2 * c3 - s1 * c2 * s3;
    out[3] = c1c2 * c3 - s1s2 * s3; //x, y, z, w

    return out;
}

// Constrain value to be inside the range [min, max] if it is outside
// of it, by returning the closest boundary; otherwise return value.
APP.Math.clamp = function(value, min, max) {
    return (value < min ? min : (value > max ? max : value));
}

// Source : three.js -> euler.js -> setFromRotationMatrix
APP.Math.makeEulerFromQuat = function(quat) {
    var out = vec3.create();

    var rotation = mat3.fromQuat(mat3.create(), quat);

    var clamp = APP.Math.clamp;

    var m11 = rotation[0],
        m12 = rotation[4],
        m13 = rotation[8];
    var m22 = rotation[5],
        m23 = rotation[9];
    var m32 = rotation[6],
        m33 = rotation[10];

    out[1] = Math.asin(clamp(m13, -1, 1));

    if (Math.abs(m13) < 0.99999) {

        out[0] = Math.atan2(-m23, m33);
        out[2] = Math.atan2(-m12, m11);

    } else {

        out[0] = Math.atan2(m32, m22);
        out[2] = 0;
    }

    return out;
}

//VVVVVVVVVVVVVVVVVVVVVV VEC2 VVVVVVVVVVVVVVVVVVVVVVVVVVVVVV//
// Define a simple vec2 class that inherits from APP.Events.
APP.Math.__Vec2 = function(x, y) {
    this._x = x || 0.0;
    this._y = y || 0.0;
}

APP.Math.__Vec2.prototype = {
    constructor: APP.Math.__Vec2,

    _x: 0,
    _y: 0,

    get x() {
        return this._x;
    },

    get y() {
        return this._y;
    },

    get xy() {
        return this.getFloat32Array();
    },

    set x(value) {
        this._x = value;
        this.trigger('change');
    },

    set y(value) {
        this._y = value;
        this.trigger('change');
    },

    set xy(array) {
        this._x = array[0];
        this._y = array[1];
    },

    getFloat32Array: function() {
        return vec2.fromValues(this._x, this._y);
    },
        //opérations de base (AMAURY)
    multiplyV:function(v, num) {
        return vec2.fromValues(v[0] * num, v[1] * num);
    },

    divideV:function(v, num) {
        return vec2.fromValues(v[0] / num, v[1] / num);
    },

    addV:function(a, b) {
        return vec2.fromValues(a[0] + b[0], a[1] + b[1]);
    },

    minusV:function(a, b) {
        return vec2.fromValues(a[0] - b[0], a[1] - b[1]);
    },

    lengthV: function (a, b) {
        return Math.sqrt(Math.pow(a[0] - b[0], 2) +
                         Math.pow(a[1] - b[1], 2));
    },
};

// This simple vector class is able to use events module.
APP.Math.Vec2 = Compose(APP.Math.__Vec2, APP.Events);

//VVVVVVVVVVVVVVVVVVVVVV VEC3 VVVVVVVVVVVVVVVVVVVVVVVVVVVVVV//
// Define a simple vec3 class that inherits from APP.Events.
APP.Math.__Vec3 = function (x, y, z) {
    this._x = x || 0.0;
    this._y = y || 0.0;
    this._z = z || 0.0;
}

APP.Math.__Vec3.prototype = {
    constructor: APP.Math.__Vec3,

    _x: 0,
    _y: 0,
    _z: 0,

    get x() {
        return this._x;
    },

    get y() {
        return this._y;
    },

    get z() {
        return this._z;
    },

    get xyz() {
        return this.getFloat32Array();
    },

    set x(value) {
        this._x = value;
        this.trigger('change');
    },

    set y(value) {
        this._y = value;
        this.trigger('change');
    },

    set z(value) {
        this._z = value;
        this.trigger('change');
    },

    set xyz(array) {
        this._x = array[0];
        this._y = array[1];
        this._z = array[2];
    },

    getFloat32Array: function () {
        return vec3.fromValues(this._x, this._y, this._z);
    },
    //opérations de base (AMAURY)
    multiplyV: function (v, num) {
        return vec3.fromValues(v[0] * num, v[1] * num, v[2] * num);
    },

    divideV: function (v, num) {
        return vec3.fromValues(v[0] / num, v[1] / num, v[2] / num);
    },

    addV: function (a, b) {
        return vec3.fromValues(a[0] + b[0], a[1] + b[1], a[2] + b[2]);
    },

    minusV: function (a, b) {
        return vec3.fromValues(a[0] - b[0], a[1] - b[1], a[2] - b[2]);
    },

    lengthV: function (a, b) {
        return Math.sqrt(Math.pow(a[0] - b[0], 2) +
                         Math.pow(a[1] - b[1], 2) +
                         Math.pow(a[2] - b[2], 2));
    },

    dotProductV: function (a, b) {
        return Math.sqrt(a[0] * b[0] +
                         a[1] * b[1] +
                         a[2] * b[2]);
    },

    crossProductV: function (a, b) {
        return vec3.fromValues( a[1] * b[2] - a[2] * b[1],
                                a[2] * b[0] - a[0] * b[2],
                                a[0] * b[1] - a[1] * b[0]);
    },
};

// This simple vector class is able to use events module.
APP.Math.Vec3 = Compose(APP.Math.__Vec3, APP.Events);

//VVVVVVVVVVVVVVVVVVVVVV VEC4 VVVVVVVVVVVVVVVVVVVVVVVVVVVVVV//
// Define a simple vec4 class that inherits from APP.Events.
APP.Math.__Vec4 = function (x, y, z, w) {
    this._x = x || 0.0;
    this._y = y || 0.0;
    this._z = z || 0.0;
    this._w = w || 0.0;
}

APP.Math.__Vec4.prototype = {
    constructor: APP.Math.__Vec4,

    _x: 0,
    _y: 0,
    _z: 0,
    _w: 0,

    get x() {
        return this._x;
    },

    get y() {
        return this._y;
    },

    get z() {
        return this._z;
    },

    get w() {
        return this._w;
    },

    get xyzw() {
        return this.getFloat32Array();
    },

    set x(value) {
        this._x = value;
        this.trigger('change');
    },

    set y(value) {
        this._y = value;
        this.trigger('change');
    },

    set z(value) {
        this._z = value;
        this.trigger('change');
    },

    set w(value) {
        this._w = value;
        this.trigger('change');
    },

    set xyzw(array) {
        this._x = array[0];
        this._y = array[1];
        this._z = array[2];
        this._w = array[3];
    },

    getFloat32Array: function () {
        return vec4.fromValues(this._x, this._y, this._z, this._w);
    },
    //opérations de base (AMAURY)
    multiplyV: function (v, num) {
        return vec4.fromValues(v[0] * num, v[1] * num, v[2] * num, v[3] * num);
    },

    divideV: function (v, num) {
        return vec4.fromValues(v[0] / num, v[1] / num, v[2] / num, v[3] / num);
    },

    addV: function (a, b) {
        return vec4.fromValues(a[0] + b[0], a[1] + b[1], a[2] + b[2], a[3] + b[3]);
    },

    minusV: function (a, b) {
        return vec4.fromValues(a[0] - b[0], a[1] - b[1], a[2] - b[2], a[3] - b[3]);
    },

    lengthV: function (a, b) {
        return Math.sqrt(Math.pow(a[0] - b[0], 2) +
                         Math.pow(a[1] - b[1], 2) +
                         Math.pow(a[2] - b[2], 2) +
                         Math.pow(a[3] - b[3], 2));
    },
};

// This simple vector class is able to use events module.
APP.Math.Vec4 = Compose(APP.Math.__Vec4, APP.Events);

// Same as APP.Math.Vec3, but components are Euler angles.
APP.Math.Euler = Compose(APP.Math.Vec3, function() {

    // Keep getters already defined.

    Object.defineProperties(this, {
        'x': {
            get: this.__lookupGetter__('x'),
            set: function(value) {
                this._x = value % 360.0;
                this.trigger('change');
            }
        },
        'y': {
            get: this.__lookupGetter__('y'),
            set: function(value) {
                this._y = value % 360.0;
                this.trigger('change');
            }
        },
        'z': {
            get: this.__lookupGetter__('z'),
            set: function(value) {
                this._z = value % 360.0;
                this.trigger('change');
            }
        },
        'xyz': {
            get: this.__lookupGetter__('xyz'),
            set: function(array) {
                this._x = array[0] % 360.0;
                this._y = array[1] % 360.0;
                this._z = array[2] % 360.0;
                this.trigger('change');
            }
        }
    });

});

// Extract translation components in m into a vec3.
APP.Math.extractTranslation = function(m) {
    if (!(m instanceof Float32Array)) {
        console.log('Matrix must be in Float32Array form.');
        return;
    }

    return vec3.fromValues(m[12], m[13], m[14]);
}

APP.Math.removeTranslation = function(m) {
    if (!(m instanceof Float32Array)) {
        console.log('Matrix must be in Float32Array form.');
        return;
    }

    var noT = mat4.clone(m);
    noT[12] = 0.0;
    noT[13] = 0.0;
    noT[14] = 0.0;

    return noT;
}

APP.Math.Curves = APP.Math.Curves ||  {};

APP.Math.Curves.BezierCubic = function(params) {

    var defaults = {
        // Control points.
        P0: vec3.fromValues(0.0, 0.0, 0.0),
        P1: vec3.fromValues(0.0, 0.0, 0.0),
        P2: vec3.fromValues(1.0, 1.0, 1.0),
        P3: vec3.fromValues(1.0, 1.0, 1.0)
    };

    var _controlPoints = _.extend(defaults, _.clone(params));

    var _coefficients = {
        B30: function(t) {
            return Math.pow(1 - t, 3);
        },
        B31: function(t) {
            return 3 * t * Math.pow(1 - t, 2);
        },
        B32: function(t) {
            return 3 * Math.pow(t, 2) * (1 - t);
        },
        B33: function(t) {
            return Math.pow(t, 3);
        }
    }

    var self = this;

    Object.defineProperty(this, 'controlPoints', {
        get: function() {
            return _controlPoints;
        },
        set: function(value) {
            _controlPoints = value;
        }
    });

    _.each(['P0', 'P1', 'P2', 'P3'], function(prop) {
        Object.defineProperty(self, prop, {
            get: function() {
                return _controlPoints[prop];
            },
            set: function(value) {
                _controlPoints[prop] = value;
            }
        });
    });

    this.getPoint = function(t) {
        t = APP.Math.clamp(t, 0.0, 1.0);

        var v0 = vec3.scale(vec3.create(), _controlPoints.P0, _coefficients.B30(t));
        var v1 = vec3.scale(vec3.create(), _controlPoints.P1, _coefficients.B31(t));
        var v2 = vec3.scale(vec3.create(), _controlPoints.P2, _coefficients.B32(t));
        var v3 = vec3.scale(vec3.create(), _controlPoints.P3, _coefficients.B33(t));

        var result = _.reduce([v0, v1, v2, v3], function(acc, v) {
            return vec3.add(acc, acc, v);
        }, vec3.create());

        return result;
    }

}

APP.Math.Surfaces = APP.Math.Surfaces ||  {};

APP.Math.Surfaces.CoonsSurface = function(params) {

    var corner1 = vec3.fromValues(0.0, 0.0, 0.0);
    var corner2 = vec3.fromValues(1.0, 0.0, 0.0);
    var corner3 = vec3.fromValues(0.0, 1.0, 0.0);
    var corner4 = vec3.fromValues(1.0, 1.0, 0.0);

    var defaults = {
        C1: new APP.Math.Curves.BezierCubic({
            P0: vec3.clone(corner1),
            P1: vec3.clone(corner1),
            P2: vec3.clone(corner2),
            P3: vec3.clone(corner2)
        }),
        C2: new APP.Math.Curves.BezierCubic({
            P0: vec3.clone(corner3),
            P1: vec3.clone(corner3),
            P2: vec3.clone(corner4),
            P3: vec3.clone(corner4)
        }),
        C3: new APP.Math.Curves.BezierCubic({
            P0: vec3.clone(corner1),
            P1: vec3.clone(corner1),
            P2: vec3.clone(corner3),
            P3: vec3.clone(corner3)
        }),
        C4: new APP.Math.Curves.BezierCubic({
            P0: vec3.clone(corner2),
            P1: vec3.clone(corner2),
            P2: vec3.clone(corner4),
            P3: vec3.clone(corner4)
        })
    };

    var _curves = _.extend(defaults, _.clone(params));

    var self = this;

    Object.defineProperty(this, 'curves', {
        get: function() {
            return _curves;
        },
        set: function(value) {
            _curves = value;
            checkFourCorners();
        }
    });

    _.each(['C1', 'C2', 'C3', 'C4'], function(prop) {
        Object.defineProperty(self, prop, {
            get: function() {
                return _curves[prop];
            },
            set: function(value) {
                _curves[prop] = value;
                checkFourCorners();
            }
        });
    });

    function checkFourCorners() {
        _curves.C1.P0 = _curves.C3.P0;
        _curves.C1.P3 = _curves.C4.P0;
        _curves.C2.P0 = _curves.C3.P3;
        _curves.C2.P3 = _curves.C4.P3;
    }

    checkFourCorners();

    function lerpU(u, v) {
        var v1 = vec3.scale(vec3.create(), _curves.C1.getPoint(u), (1 - v));
        var v2 = vec3.scale(vec3.create(), _curves.C2.getPoint(u), v);
        return vec3.add(v1, v1, v2);
    }

    function lerpV(u, v) {
        var v1 = vec3.scale(vec3.create(), _curves.C3.getPoint(v), (1 - u));
        var v2 = vec3.scale(vec3.create(), _curves.C4.getPoint(v), u);
        return vec3.add(v1, v1, v2);
    }

    function blerp(u, v) {
        var v1 = vec3.scale(vec3.create(), _curves.C1.getPoint(0), (1 - u) * (1 - v));
        var v2 = vec3.scale(vec3.create(), _curves.C1.getPoint(1), u * (1 - v));
        var v3 = vec3.scale(vec3.create(), _curves.C2.getPoint(0), (1 - u) * v);
        var v4 = vec3.scale(vec3.create(), _curves.C2.getPoint(1), u * v);

        var result = _.reduce([v1, v2, v3, v4], function(acc, v) {
            return vec3.add(acc, acc, v);
        }, vec3.create());

        return result;
    }

    this.getPoint = function(u, v) {
        u = APP.Math.clamp(u, 0.0, 1.0);
        v = APP.Math.clamp(v, 0.0, 1.0);

        var v1 = vec3.add(vec3.create(), lerpU(u, v), lerpV(u, v));

        return vec3.sub(v1, v1, blerp(u, v));
    }

}

APP.Math.Plane = function(params) {

    var defaults = {
        normal: vec3.fromValues(1, 0, 0),
        constant: 0
    };

    var _attrs = _.extend(defaults, _.clone(params));

    var self = this;

    Object.defineProperties(this, {
        'normal': {
            get: function() {
                return _attrs.normal;
            },
            set: function(value) {
                _attrs.normal = vec3.clone(value);
                vec3.normalize(_attrs.normal, _attrs.normal);
            }
        },
        'constant': {
            get: function() {
                return _attrs.constant;
            },
            set: function(value) {
                _attrs.constant = value;
            }
        },
        'xyzw': {
            get: function() {
                return vec4.fromValues(
                    _attrs.normal[0],
                    _attrs.normal[1],
                    _attrs.normal[2],
                    _attrs.constant
                );
            },
            set: function(value) {
                var v = vec4.fromValues(value[0], value[1], value[2], value[3]);
                vec4.normalize(v, v);
                _attrs.normal = vec3.fromValues(v[0], v[1], v[2]);
                _attrs.constant = v[3];
            }
        }
    });

    this.distanceToPoint = function(p) {
        // Hessian normal form of a plane.
        return vec3.dot(this.normal, p) + this.constant;
    }

}

APP.Math.Sphere = function(params) {

    var defaults = {
        center: vec3.create(),
        radius: 0
    };

    var _attrs = _.extend(defaults, _.clone(params));

    var self = this;
    _.each(['center', 'radius'], function(prop) {
        Object.defineProperty(self, prop, {
            get: function() {
                return _attrs[prop];
            },
            set: function(value) {
                _attrs[prop] = value;
            }
        });
    });

    this.clone = function() {
        return new APP.Math.Sphere(vec3.clone(this.center), this.radius);
    }

    this.setMinimalSphereContainingPoints = function(points) {
        var self = this;
        this.radius = _.reduce(points, function(acc, p) {
            acc = Math.max(acc, vec3.dist(self.center, p));
        }, 0);

        return this;
    }

    this.applyTransformations = function(matrix) {
        if (!_.isUndefined(matrix)) {
            // Apply matrix transformations on bounding sphere.
            vec3.transformMat4(_attrs.center, _attrs.center, matrix);

            // Scale the radius.
            var m3 = mat3.fromMat4(mat3.create(), matrix);
            var rows = [
                vec3.fromValues(m3[0], m3[1], m3[2]),
                vec3.fromValues(m3[3], m3[4], m3[5]),
                vec3.fromValues(m3[6], m3[7], m3[8]),
            ];

            // Get maximum length from vec3 in rows.
            var maxScaling = _.reduce(rows, function(max, r) {
                return Math.max(max, vec3.len(r));
            }, 0);

            _attrs.radius = _attrs.radius * maxScaling;
        }
    }

}

APP.Math.Frustum = function(params) {

    var defaults = {};
    _.each(['P0', 'P1', 'P2', 'P3', 'P4', 'P5'], function(p) {
        defaults[p] = new APP.Math.Plane();
    });

    var _attrs = _.extend(defaults, _.clone(params));
    _attrs.planes = [_attrs.P0, _attrs.P1, _attrs.P2, _attrs.P3, _attrs.P4, _attrs.P5];

    this.setFromMatrix = function(m) {
        // Source for equations : Three.js -> Frustum.js
        // https://github.com/mrdoob/three.js/blob/master/src/math/Frustum.js
        _attrs.P0.xyzw = vec4.fromValues(m[3] - m[0], m[7] - m[4], m[11] - m[8], m[15] - m[12]);
        _attrs.P1.xyzw = vec4.fromValues(m[3] + m[0], m[7] + m[4], m[11] + m[8], m[15] + m[12]);
        _attrs.P2.xyzw = vec4.fromValues(m[3] + m[1], m[7] + m[5], m[11] + m[9], m[15] + m[13]);
        _attrs.P3.xyzw = vec4.fromValues(m[3] - m[1], m[7] - m[5], m[11] - m[9], m[15] - m[13]);
        _attrs.P4.xyzw = vec4.fromValues(m[3] - m[2], m[7] - m[6], m[11] - m[10], m[15] - m[14]);
        _attrs.P5.xyzw = vec4.fromValues(m[3] + m[2], m[7] + m[6], m[11] + m[10], m[15] + m[14]);

        return this;
    }

    this.containsSphere = function(s) {
        // isOutside will be false if s is on the inside halfspace of each plane.
        // _.find() will return undefined if the predicate returns false for each value.
        var isOutside = _.isUndefined(_.find(_attrs.planes, function(p) {
            return (p.distanceToPoint(s.center) < -s.radius);
        }));

        return isOutside;
    }

}
