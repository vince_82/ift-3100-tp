APP.__Renderer = function(options) {

    APP.PERSPECTIVE = 'perspective';
    APP.ORTHOGONAL = 'orthogonal';

    var defaults = {
        projection: APP.PERSPECTIVE
    };

    this.options = APP.Utils.copyToAndUpdate(defaults, options);

    var gl = APP.WebGLContext;

    var viewMatrix;
    var projMatrix;
    var _frustum = new APP.Math.Frustum();

    var _compileShaders = function() {
        APP.Shaders = APP.Shaders ||  {};

        _.each(APP.uncompiledShaders, function(us, key) {
            APP.Shaders[key] = new APP.Shader({
                vsUniforms: us.vsUniforms,
                fsUniforms: us.fsUniforms,
                attributes: us.attributes,
                vertexShaderSourceIsTemplate: us.vertexShaderSourceIsTemplate,
                fragmentShaderSourceIsTemplate: us.fragmentShaderSourceIsTemplate,
                vertexShaderSource: us.vertexShaderSourceGetFunc(), // It's a callback because the DOM must be loaded
                fragmentShaderSource: us.fragmentShaderSourceGetFunc()
            })
        });
    }

    this.init = function(camera) {
        var target = vec3.fromValues(0, 0, 0);
        viewMatrix = camera.lookAtTarget(target, false);

        projMatrix =
            (this.options.projection == APP.PERSPECTIVE) ? camera.perspectiveProjection() : camera.orthographicProjection();
        var viewMatrix = camera.lookAtTarget(target, false);


        gl.enable(gl.DEPTH_TEST); // Enable depth testing.

        _compileShaders();
    }



    this.lookAt = function(camera, target, gradual) {
        //Setting a non-null 'target' is the same as changing the camera's 'focus' object.
        viewMatrix = camera.lookAtTarget(target, false);
    }

    this.projection = function(camera) {
        projMatrix =
            (this.options.projection == APP.PERSPECTIVE) ? camera.perspectiveProjection() : camera.orthographicProjection();
    }

    this.render = function(scene, camera) {
    	var defaultShader = APP.Shaders.defaultShader;

        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

        camera.cameraEventManager(); //Take in UserInput + friction
        this.lookAt(camera); //Update View Matrix
        this.projection(camera); //Update Projection Matrix


        // Update world matrix and children matrices before drawing scene.
        // This call will go in the update method of the app eventually.
        scene.updateWorldMatrix();

        defaultShader.useProgram();

        // Bind uniforms
        defaultShader.uniforms['ProjMatrix'].value = projMatrix;
        defaultShader.uniforms['ViewMatrix'].value = viewMatrix;
        defaultShader.uniforms['cameraPosition'].value = camera.position;


        scene.draw({
            defaultShader: defaultShader,
            viewMatrix: viewMatrix,
            projMatrix: projMatrix,
            camera: camera,
            frustum: _frustum.setFromMatrix(mat4.mul(mat4.create(), projMatrix, viewMatrix))
        });
    }

}

// Extend with events module.
APP.Renderer = Compose(APP.__Renderer, APP.Events);
