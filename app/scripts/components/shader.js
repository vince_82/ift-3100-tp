APP.Shader = function(params) {

    var defaults = {
        vsUniforms: undefined,
        fsUniforms: undefined,
        attributes: [],
        vertexShaderSource: undefined,
        fragmentShaderSource: undefined,
        autocompile: true,
        vertexShaderSourceIsTemplate: true,
        fragmentShaderSourceIsTemplate: true,
        addVertexShaderDeclarationBlocks: true,
        addFragmentShaderDeclarationBlocks: true,
        templateVariable: 'vars'
    };

    var _gl = APP.WebGLContext;
    var _attrs = APP.Utils.copyToAndUpdate(defaults, params);

    if (!_.isUndefined(_attrs.vsUniforms) && !_.isUndefined(_attrs.fsUniforms)) {
        _attrs.uniforms = _attrs.vsUniforms.union(_attrs.fsUniforms);
    }

    Object.defineProperty(this, 'uniforms', {
        get: function() {
            return _attrs.uniforms;
        }
    });

    var self = this;

    _.each(_.keys(_.omit(_attrs, 'uniforms')), function(prop) {
        Object.defineProperty(self, prop, {
            get: function() {
                return _attrs[prop];
            }
        })
    });

    _attrs.program = undefined;
    Object.defineProperty(this, 'program', {
        get: function() {
            return _attrs.program;
        }
    });

    var compile = function(source, type) {
        var shader = _gl.createShader(type);
        _gl.shaderSource(shader, source);
        _gl.compileShader(shader);

        if (_gl.getShaderParameter(shader, _gl.COMPILE_STATUS) == 0)
            alert(_gl.getShaderInfoLog(shader));

        return {
            shader: shader,
            status: _gl.getShaderParameter(shader, _gl.COMPILE_STATUS),
            log: _gl.getShaderInfoLog(shader)
        };
    }

    var link = function(vShader, fShader) {
        var prog = _gl.createProgram();

        _gl.attachShader(prog, vShader);
        _gl.attachShader(prog, fShader);
        _gl.linkProgram(prog);

        return {
            prog: prog,
            status: _gl.getProgramParameter(prog, _gl.LINK_STATUS),
            log: _gl.getProgramInfoLog(prog)
        }
    }

    var _compileProgram = function(vsSource, fsSource) {

        var vs = compile(vsSource, _gl.VERTEX_SHADER);
        var fs = compile(fsSource, _gl.FRAGMENT_SHADER);
        var prog;

        if (vs.status == 1 && fs.status == 1) {
            prog = link(vs.shader, fs.shader);

            if (prog.status == 0) {
                console.log('Unable to link programs.');
            }
        }

        return prog;
    }

    this.renderTemplate = function(strTemplate, data) {
        // Compile source string template into usable shader source code.
        var compiled = _.template(strTemplate, {
            variable: this.templateVariable
        });

        return compiled(data);
    }

    var addVertexShaderUniformsDeclarationBlocks = function() {
        if (self.addVertexShaderDeclarationBlocks) {
            return [
                self.attributes.toString(),
                self.vsUniforms.toString()
            ].join('\n');
        } else {
            return '';
        }
    }

    var addFragmentShaderUniformsDeclarationBlock = function() {
        if (self.addFragmentShaderDeclarationBlocks) {
            return self.fsUniforms.toString();
        } else {
            return '';
        }
    }

    this.compileVertexShaderSourceTemplate = function() {
        this.vertexShaderSourceCompiled = this.renderTemplate([
            addVertexShaderUniformsDeclarationBlocks(),
            this.vertexShaderSource
        ].join('\n'), _.extend(
            this.vsUniforms.namesToJSON(),
            this.attributes.namesToJSON()
        ));
    }

    this.compileFragmentShaderSourceTemplate = function() {
        this.fragmentShaderSourceCompiled = this.renderTemplate([
                addFragmentShaderUniformsDeclarationBlock(),
                this.fragmentShaderSource
            ].join('\n'),
            this.fsUniforms.namesToJSON()
        );
    }

    this.compileProgram = _compileProgram;

    var attachVariablesLocations = function() {
        _.each(self.uniforms, function(u) {
            u.location = _gl.getUniformLocation(self.program, u.completeName);
        });

        _.each(self.attributes, function(a) {
            a.location = _gl.getAttribLocation(self.program, a.name);
        });
    }

    var _makeProgram = function() {

        if (self.vertexShaderSourceIsTemplate) {
            self.compileVertexShaderSourceTemplate();
        } else {
            self.vertexShaderSourceCompiled = self.vertexShaderSource;
        }

        if (self.fragmentShaderSourceIsTemplate) {
            self.compileFragmentShaderSourceTemplate();
        } else {
            self.fragmentShaderSourceCompiled = self.fragmentShaderSource;
        }

        var result = _compileProgram(
            self.vertexShaderSourceCompiled,
            self.fragmentShaderSourceCompiled
        );
        
        _attrs.program = result.prog;

        attachVariablesLocations();

        return _attrs.program;

    }

    this.useProgram = function() {
        _gl.useProgram(_attrs.program);
    }

    // Compile shader program.
    if (this.autocompile) _makeProgram();

}
