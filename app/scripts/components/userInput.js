APP.UserInput = function(options) {
	/* basic camera class */
	
	var defaults = {};

	this.options = APP.Utils.copyToAndUpdate(defaults, options);
	
	this.keyPressed = new Array(1024);
	
	for ( var i = 0; i < 1024; i++ )
		this.keyPressed[i] = 0;
	
	this.mouseRightPressed = false;
	this.mouseLeftPressed = false;
	this.mouseScrollDeltaY = 0.0;
	this.mouseX = 0.0;
	this.mouseY = 0.0;
	this.mouseDeltaX = 0.0;
	this.mouseDeltaY = 0.0;
	
	var self = this;
	var interval;
	var interval2;
	
	$( window ).keydown(function(event) {
		self.keyPressed[event.keyCode] = true;
	});
	$( window ).keyup(function(event) {
		self.keyPressed[event.keyCode] = false;
	});
	
	$( "#canvas" ).mousedown(function(event) {
		switch (event.which) {
			case 1:
				self.mouseLeftPressed = true;
				break;
			case 3:
				self.mouseRightPressed = true;
				break;
		}
	});
	$( "#canvas" ).mouseup(function(event) {
		switch (event.which) {
			case 1:
				self.mouseLeftPressed = false;
				break;
			case 3:
				self.mouseRightPressed = false;
				break;
		}
	});
	
	$("#canvas").mousewheel(function(event) {
		clearInterval(interval);
        self.mouseScrollDeltaY = event.deltaY;
		interval = setInterval(function() {
			self.mouseScrollDeltaY = 0;
			clearInterval(interval);
		}, 20);
    });
	
	$('#canvas').mousemove(function(event){
		clearInterval(interval2);
		self.mouseDeltaX = event.pageX - self.mouseX;
		self.mouseDeltaY = event.pageY - self.mouseY;
        self.mouseX = event.pageX;
        self.mouseY = event.pageY;
		interval2 = setInterval(function() {
			self.mouseDeltaX = 0;
			self.mouseDeltaY = 0;
			clearInterval(interval2);
		}, 200);
    });
	
}

// Extend with events module.
Compose(APP.UserInput, APP.Events);