// ========================================
// Geometry ctor.
// params : object containing 
//     vertices: array of vec3 (Float32Array)
//     colors:   array of vec4 (Float32Array) 
//     indices:  Float32Array
//     uvs:      array of vec2 (Float32Array)
// 
// This class manages a VAO internally.
// 
// TODO : use faces instead of indices.
// ========================================

APP.__Geometry = function(params) {

    this.gl = APP.WebGLContext;

    var defaults = {
        vertices: [],
        colors: [],
        indices: [],
        normals: [],
        uvs: [],
        drawMode: this.gl.TRIANGLES,
        boundingSphere: new APP.Math.Sphere(),
        shader: APP.Shaders.defaultShader,
        useVAO: true
    };

    var _attrs = _.extend(defaults, params);

    // If only one color is passed instead of one color by vertex,
    // we duplicate the color for each vertex.
    if ((_attrs.colors.length == 1) && (_attrs.colors.length != _attrs.vertices.length)) {
        var color = _attrs.colors[0];
        // Push a color (_attrs.vertices.length - 1) times,
        // because there's already one in the array.
        _.times(_attrs.vertices.length - 1, function(n) {
            _attrs.colors.push(color);
        });
    }

    // Getters and setters.

    var self = this;

    _.each(_.keys(_.omit(_attrs, ['drawMode'])), function(prop) {
        Object.defineProperty(self, prop, {
            get: function() {
                return _attrs[prop];
            },
            set: function(value) {
                _attrs[prop] = value;
                self.trigger('change:' + prop);
            }
        });
    });

    Object.defineProperty(this, 'drawMode', {
        get: function() {
            return _attrs.drawMode
        },
        set: function(value) {
            _attrs.drawMode = value;
        }
    })

    this.bindVAO = function() {
        APP.WebGLExtensions.VAO.bindVertexArrayOES(this.vao);
    }

    this.unbindVAO = function() {
        APP.WebGLExtensions.VAO.bindVertexArrayOES(null);
    }

    this.sendDataToBuffer = function(bufferType, buffer, data, mode) {
        var gl = this.gl;
        gl.bindBuffer(bufferType, buffer);
        gl.bufferData(bufferType, data, mode);
    }

    this.setPointerToVertexAttribute = function(attribute, itemSize, dataType, stride) {
        var gl = this.gl;

        // Set pointer to vertex attribute.
        if (attribute < 0) {
            console.log('set pointer attribute of negative attribute');
            console.trace();
        }


        gl.vertexAttribPointer(attribute, itemSize, dataType, false, stride, 0);
        gl.enableVertexAttribArray(attribute);
    }

    this.sendVerticesDataToBuffer = function() {
        var gl = self.gl;

        // Get the vertices from the two vec3 objects and concatenate inside one array.
        var verticesFlatten = APP.Utils.Float32ConcatMultiple(self.vertices);

        self.sendDataToBuffer(gl.ARRAY_BUFFER, self.vertexBuffer, verticesFlatten, gl.STATIC_DRAW);
        // Set pointer to vertex attribute.
        var posAttr = self.shader.attributes['position'].location;
        self.setPointerToVertexAttribute(posAttr, 3, gl.FLOAT, 0);

    }

    this.sendColorsDataToBuffer = function() {
        var gl = self.gl;
        var colorAttr = self.shader.attributes['color'].location;

        if (self.colors.length > 0 && colorAttr >= 0) {

            // Get the colors from the vec3 objects and flatten in one array.
            var colorsFlatten = APP.Utils.Float32ConcatMultiple(self.colors);

            self.colorBuffer = self.colorBuffer || gl.createBuffer();
            self.sendDataToBuffer(gl.ARRAY_BUFFER, self.colorBuffer, colorsFlatten, gl.STATIC_DRAW);

            // Set pointer to vertex attribute.
            self.setPointerToVertexAttribute(colorAttr, 4, gl.FLOAT, 0);

        } else {

            if (colorAttr >= 0) gl.disableVertexAttribArray(colorAttr);

        }

    }

    this.sendUvsDataToBuffer = function() {
        var gl = self.gl;
        var uvsAttr = self.shader.attributes['uv'].location;

        if (self.uvs.length > 0 && uvsAttr >= 0) {

            var uvs = APP.Utils.Float32ConcatMultiple(self.uvs);
            self.uvBuffer = self.uvBuffer || gl.createBuffer();
            self.sendDataToBuffer(gl.ARRAY_BUFFER, self.uvBuffer, uvs, gl.STATIC_DRAW);

            // Set pointer to vertex attribute.
            self.setPointerToVertexAttribute(uvsAttr, 2, gl.FLOAT, 0);

        } else {

            if (uvsAttr >= 0) gl.disableVertexAttribArray(uvsAttr);

        }

    }

    this.sendIndicesDataToBuffer = function() {
        var gl = self.gl;

        if (self.indices.length > 0) {

            var indices = new Uint16Array(self.indices);
            self.indexBuffer = self.indexBuffer || gl.createBuffer();
            self.sendDataToBuffer(gl.ELEMENT_ARRAY_BUFFER, self.indexBuffer, indices, gl.STATIC_DRAW);

        }

    }

    this.sendNormalsDataToBuffer = function() {
        var gl = self.gl;
        var normalAttr = self.shader.attributes['normal'].location;

        if (self.normals.length > 0 && normalAttr >= 0) {

            var normals = APP.Utils.Float32ConcatMultiple(self.normals);
            self.normalBuffer = self.normalBuffer || gl.createBuffer();
            self.sendDataToBuffer(gl.ARRAY_BUFFER, self.normalBuffer, normals, gl.STATIC_DRAW);

            // Set pointer to vertex attribute.
            self.setPointerToVertexAttribute(normalAttr, 3, gl.FLOAT, 0);

        } else {

            if (normalAttr >= 0) gl.disableVertexAttribArray(normalAttr);

        }

    }

    this._wrapInVAOActivation = function(fun) {
        return _.wrap(fun, function(wrappedFun) {
            self.bindVAO();
            wrappedFun();
            self.unbindVAO();
        });
    }

    this.bindBuffers = function() {

        var gl = this.gl;

        var posAttr = this.shader.attributes['position'].location;
        var normalAttr = this.shader.attributes['normal'].location;
        var uvsAttr = this.shader.attributes['uv'].location;
        var colorAttr = self.shader.attributes['color'].location;

        // Vertices
        gl.bindBuffer(gl.ARRAY_BUFFER, this.vertexBuffer);
        this.setPointerToVertexAttribute(posAttr, 3, gl.FLOAT, 0);


        // Normals
        if (this.normals.length > 0 && normalAttr >= 0) {

            gl.bindBuffer(gl.ARRAY_BUFFER, this.normalBuffer);
            this.setPointerToVertexAttribute(normalAttr, 3, gl.FLOAT, 0);

        } else {

            if (normalAttr >= 0) gl.disableVertexAttribArray(normalAttr);

        }


        // Uvs
        if (this.uvs.length > 0 && uvsAttr >= 0) {

            gl.bindBuffer(gl.ARRAY_BUFFER, this.uvBuffer);
            this.setPointerToVertexAttribute(uvsAttr, 2, gl.FLOAT, 0);

        } else {

             if (uvsAttr >= 0) gl.disableVertexAttribArray(uvsAttr);

        }

        // Colors
        if (this.colors.length > 0 && colorAttr >= 0) {

            gl.bindBuffer(gl.ARRAY_BUFFER, this.colorBuffer);
            this.setPointerToVertexAttribute(normalAttr, 4, gl.FLOAT, 0);

        } else {

            if (colorAttr >= 0) gl.disableVertexAttribArray(colorAttr);

        }


        // 
        if (this.indices.length > 0) {

            gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.indexBuffer);

        }

    }

    this._drawElements = function() {
        var gl = self.gl;

        if (this.useVAO) {

            self._wrapInVAOActivation(function() {
                // Draw the geometry.
                gl.drawElements(_attrs.drawMode, self.indices.length, gl.UNSIGNED_SHORT, 0);
            })();

        } else {

            this.bindBuffers();

            // Draw the geometry.
            gl.drawElements(_attrs.drawMode, self.indices.length, gl.UNSIGNED_SHORT, 0);

        }

    };

    this._drawArrays = function() {
        var gl = self.gl;

        if (this.useVAO) {

            self._wrapInVAOActivation(function() {
                // Draw the geometry.
                gl.drawArrays(_attrs.drawMode, 0, self.vertices.length);
            })();

        } else {

            this.bindBuffers();

            // Draw the geometry.
            gl.drawArrays(_attrs.drawMode, 0, self.vertices.length);

        }
    }

    this.draw = function(mode) {
        if (!_.isUndefined(mode)) this.drawMode = mode;
        if (this.indices.length > 0) {
            this._drawElements();
        } else {
            this._drawArrays();
        }
    }

    this.computeNormals = function() {
        var nbFaces = this.indices.length / 3;
        if (this.normals.length == 0) this.normals = new Array(this.vertices.length);

        for (var i = 0; i < this.normals.length; ++i) {
            this.normals[i] = vec3.create();
        }

        // Compute vertex normals weighted by triangles areas.
        // Source of algorithm : Three.js -> Geometry.js
        // https://github.com/mrdoob/three.js/blob/master/src/core/Geometry.js
        // http://www.iquilezles.org/www/articles/normals/normals.htm
        for (var j = 0; j < nbFaces; ++j) {
            var jx3 = j * 3;

            var v1 = this.vertices[this.indices[jx3]];
            var v2 = this.vertices[this.indices[jx3 + 1]];
            var v3 = this.vertices[this.indices[jx3 + 2]];

            var v1v2 = vec3.sub(vec3.create(), v1, v2);
            var v3v2 = vec3.sub(vec3.create(), v3, v2);
            var cross = vec3.cross(vec3.create(), v1v2, v3v2);

            var jx3 = j * 3;
            var n1 = this.normals[this.indices[jx3]];
            var n2 = this.normals[this.indices[jx3 + 1]];
            var n3 = this.normals[this.indices[jx3 + 2]];

            vec3.add(n1, n1, cross);
            vec3.add(n2, n2, cross);
            vec3.add(n3, n3, cross);

        }

        // Normalization.
        for (var i = 0; i < this.normals.length; ++i) {
            var n = this.normals[i];
            vec3.normalize(n, n);
        }

        if (this.useVAO) {
            this._wrapInVAOActivation(this.sendNormalsDataToBuffer)();
        } else {
            this.sendNormalsDataToBuffer();
        }
    }

    this.computeBoundingSphere = function() {
        _attrs.boundingSphere.setMinimalSphereContainingPoints(_attrs.vertices);
    }

}

APP.__Geometry = Compose(APP.__Geometry, APP.Events);

var init = function() {
    // Initialization. This function is executed along with the ctor function.

    var gl = this.gl;

    // Create/bind a VAO. 
    if (this.useVAO) {

        this.vao = APP.WebGLExtensions.VAO.createVertexArrayOES();

        // Select vao as active object.  
        this.bindVAO();

    }


    //=====================================
    //  Create buffers. 
    //=====================================

    // 1. Vertex buffer.

    this.vertexBuffer = gl.createBuffer();
    this.sendVerticesDataToBuffer();

    // 2. Color buffer.

    if (this.colors.length > 0) {

        this.colorBuffer = gl.createBuffer();
        this.sendColorsDataToBuffer();

    }

    // 3. UV buffer.

    if (this.uvs.length > 0) {

        this.uvBuffer = gl.createBuffer();
        this.sendUvsDataToBuffer();

    }

    // 4. Index buffer.

    if (this.indices.length > 0) {

        this.indexBuffer = gl.createBuffer();
        this.sendIndicesDataToBuffer();

    }

    // 5. Normals buffer.

    if (this.normals.length > 0) {

        this.normalBuffer = gl.createBuffer();
        this.sendNormalsDataToBuffer();

    }

    if (this.useVAO) {
        // Unbind vao.  
        this.unbindVAO();
    }

    this.computeBoundingSphere();

    // Events wiring.

    this.listenTo(this, 'change:vertices', function() {
        self._wrapInVAOActivation(this.sendVerticesDataToBuffer)();
        // Recompute bounding sphere.
        self.computeBoundingSphere();
    });
    this.listenTo(this, 'change:colors', this._wrapInVAOActivation(this.sendColorsDataToBuffer));
    this.listenTo(this, 'change:uvs', this._wrapInVAOActivation(this.sendUvsDataToBuffer));
    this.listenTo(this, 'change:indices', this._wrapInVAOActivation(this.sendIndicesDataToBuffer));

}

APP.Geometry = Compose(APP.__Geometry, init);
