APP.Material = function(params) {

    var defaults = {
        texture: undefined,
        specularMap: undefined,
        ambient: vec4.fromValues(0.2, 0.2, 0.2, 1),
		diffuse: vec4.fromValues(0.6, 0.6, 0.6, 1),
        specular: vec4.fromValues(1, 1, 1, 1),
        shininess: 32.0,
		roughness: 0.35,
		Xroughness: 0.75,
		Yroughness: 0.15,
		
        shader: undefined,
    }

    var _attrs = _.extend(defaults, _.clone(params));

    var self = this;
    var gl = APP.WebGLContext;

    APP.Utils.defineStandardProperties(this, _attrs);

    this.bind = function(commonUniformValues) {

        this.shader.useProgram();

        if (!_.isUndefined(this.texture)) {
            // Use texture.
            this.texture.bind(gl.TEXTURE0, 0, this.shader);

        } else {
            // Use diffuse and ambient color.
            this.shader.uniforms['m_ambient'].value = this.ambient;
			this.shader.uniforms['m_diffuse'].value = this.diffuse;
        }

        if (!_.isUndefined(this.specularMap)) {
            // Use specular map.
            this.specularMap.bind(gl.TEXTURE1, 1, this.shader);

        } else {

            this.shader.uniforms['m_specular'].value = this.specular;

        }

        this.shader.uniforms['m_shininess'].value = this.shininess;
		this.shader.uniforms['m_roughness'].value = this.roughness;
		this.shader.uniforms['m_Xroughness'].value = this.Xroughness;
		this.shader.uniforms['m_Yroughness'].value = this.Yroughness;
		
        this.shader.uniforms.setValues(commonUniformValues);

    }

    this.unbind = function(defaultShader) {

        if (!_.isUndefined(this.texture)) this.texture.unbind();
        if (!_.isUndefined(this.specularMap)) this.specularMap.unbind();
        if (!_.isUndefined(defaultShader)) defaultShader.useProgram();

    }

}

var after = Compose.after;

APP.WaterMaterial = Compose(APP.Material, function(params) {

    this.shader = APP.Shaders['waterShader'];

    var defaults = {
        waveTime: 0.1,
        waveWidth: 1.9,
        waveHeight: 1.45,
        waveFreq: 0.02
    };

    var _attrs = _.extend(defaults, _.pick(params, _.keys(defaults)));

    APP.Utils.defineStandardProperties(this, _attrs);

    var gl = APP.WebGLContext;

    this.bind = function(commonUniformValues) {

        this.shader.useProgram();

        if (!_.isUndefined(this.texture)) {
            // Use texture.
            this.texture.bind(gl.TEXTURE0, 0, this.shader);

        } else {
            // Use diffuse and ambient color.
            this.shader.uniforms['m_ambient'].value = this.ambient;
			this.shader.uniforms['m_diffuse'].value = this.diffuse;

        }

        if (!_.isUndefined(this.specularMap)) {
            // Use specular map.
            this.specularMap.bind(gl.TEXTURE1, 1, this.shader);

        } else {

            this.shader.uniforms['m_specular'].value = this.specular;

        }

        this.shader.uniforms['m_shininess'].value = this.shininess;
		this.shader.uniforms['m_roughness'].value = this.roughness;
		this.shader.uniforms['m_Xroughness'].value = this.Xroughness;
		this.shader.uniforms['m_Yroughness'].value = this.Yroughness;

        // Bind passed in uniforms.
        this.waveTime += this.waveFreq;
        var uValues = _.extend(commonUniformValues, _.omit(_attrs, 'waveFreq'));
        this.shader.uniforms.setValues(uValues);

    }

});
