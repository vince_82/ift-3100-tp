// Ctor
APP.App = Compose(APP.Events, function(options) {

    var defaults = {
        stats: undefined,
        setup: function() {},
        update: function() {},
        draw: function() {},
        injectCanvas: false,
        animate: true
    };

    var gl;

    // Override defaults with options.
    this.options = APP.Utils.copyToAndUpdate(defaults, options);

    // Extend the current object with the main 3 functions,
    // instead of accessing it by the options object.
    Compose.call(this, _.pick(defaults, 'stats', 'setup', 'update', 'draw'));

    var _animate = this.options.animate;
    Object.defineProperty(this, 'animate', {
        get: function() {
            return _animate;
        },

        set: function(value) {
            _animate = value;

        }
    })

    var canvas;
    if (!defaults.canvas) {
        // Create a <canvas> if no canvas element is passed.
        canvas = document.createElement('canvas');
    } else {
        canvas = defaults.canvas;
    }

    if (defaults.injectCanvas) {
        $('body').append(canvas);
    }


    var scope = this;

    // Drawing loop.
    var _draw = function() {

        if(scope.stats) scope.stats.begin();

        scope.update(scope.animate);
        scope.draw();

        if(scope.stats) scope.stats.end();

        requestAnimationFrame(_draw);
    }

    // Start the app : setup and loop.
    this.start = function() {

        this.setup();

        // Main loop.
        requestAnimationFrame(_draw);
    };

});
