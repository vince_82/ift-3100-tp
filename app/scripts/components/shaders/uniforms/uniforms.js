APP.UniformPrefix = 'u';

APP.Uniforms =  APP.Uniforms || {};

APP.Uniforms.UniformBase = function(params) {

    var defaults = {
        name: undefined,
        type: undefined,
        shaderType: undefined,
        location: undefined,
        value: undefined,
        program: undefined,
        namePrefix: APP.UniformPrefix, // For example 'u' : in the shader it'll be : uMyUniformVar
        precision: undefined, // Like highp
        arrayLenConst: undefined,
        autocompileTemplate: true
    };

    var _attrs = _.extend(defaults, _.clone(params));
    // The value passed is kept as the default value.
    _attrs.defaultValue = _attrs.value;

    // Define getters and setters, except for the value.
    var self = this;
    _.each(_.keys(_.omit(_attrs, 'value', 'defaultValue')), function(prop) {
        Object.defineProperty(self, prop, {
            get: function() {
                return _attrs[prop]
            },
            set: function(value) {
                _attrs[prop] = value;
            }
        });
    });

    // When value is set, upload data to GPU.
    Object.defineProperties(this, {
        'value': {
            get: function() {
                return _attrs.value;
            },
            set: function(value) {
                _attrs.value = value;
                this.uploadData(); // Send data to program on GPU.
            }
        },
        'defaultValue': {
            get: function() {
                return _attrs.defaultValue;
            }
        },
        'attributes': {
            get: function() {
                return _attrs;
            }
        }
    });

    // Like a computed field.
    Object.defineProperty(this, 'completeName', {
        get: function() {

            if (!_.isUndefined(this.namePrefix)) {

                return this.namePrefix + this.name;

            } else {
                return this.name;
            }

        }
    })

    this.uploadData = function() {
        var _gl = APP.WebGLContext;

        if (!_.isUndefined(this.type) && !_.isUndefined(this.location) && !_.isUndefined(this.value)) {

            // Method to call on the WebGL context.
            // We need to bind it to its context if we do this that way,
            // because there will be no more context for the function otherwise.
            // See http://stackoverflow.com/questions/10743596/why-are-certain-function-calls-termed-illegal-invocations-in-javascript
            var glfunc = _gl['uniform' + this.type].bind(_gl);

            if (/Matrix/g.test(this.type)) {

                // type contains Matrix, there's a third parameter
                glfunc(this.location, false, this.value);

            } else {

                glfunc(this.location, this.value);
            }

        } else {

            console.warn('Uniform object definition is incomplete. Cannot send data to program.');

        }

    }

    // Like string formatting.
    this.stringTemplate =
        "uniform <%if(self.precision){%><%=self.precision%> <%}%><%=self.shaderType%> <%=self.name%><%if(self.arrayLenConst){%>[<%=self.arrayLenConst%>]<%}%>;";

    if (this.autocompileTemplate) {
        // Compile the template string into a function.
        this.template = _.template(this.stringTemplate, {
            variable: 'self'
        });
    }

    this.toString = function() {
        return this.template({
            precision: this.precision,
            shaderType: this.shaderType,
            name: this.completeName
        });
    }

    this.clone = function() {
        return new APP.Uniforms.UniformBase(
            _.clone(_.omit(_attrs, ['value', 'program', 'location']))
        );
    }

}

//============================
// Derived uniform classes defining types automatically.

APP.Uniforms.UniformSampler2D = Compose(APP.Uniforms.UniformBase, APP.Events, function() {

    this.type = '1i';
    this.shaderType = 'sampler2D'

    this.clone = function() {
        return new APP.Uniforms.UniformSampler2D(
            _.clone(_.omit(this.attributes, ['value', 'program', 'location']))
        );
    }

});

APP.Uniforms.Uniform1f = Compose(APP.Uniforms.UniformBase, APP.Events, function() {

    this.type = '1f';
    this.shaderType = 'float'

    this.clone = function() {
        return new APP.Uniforms.Uniform1f(
            _.clone(_.omit(this.attributes, ['value', 'program', 'location']))
        );
    }

});

APP.Uniforms.Uniform3f = Compose(APP.Uniforms.UniformBase, APP.Events, function() {

    this.type = '3f';
    this.shaderType = 'vec3'

    this.clone = function() {
        return new APP.Uniforms.Uniform3f(
            _.clone(_.omit(this.attributes, ['value', 'program', 'location']))
        );
    }

});

APP.Uniforms.Uniform3fv = Compose(APP.Uniforms.UniformBase, APP.Events, function() {

    this.type = '3fv';
    this.shaderType = 'vec3'

    this.clone = function() {
        return new APP.Uniforms.Uniform3fv(
            _.clone(_.omit(this.attributes, ['value', 'program', 'location']))
        );
    }

});

APP.Uniforms.UniformMatrix3fv = Compose(APP.Uniforms.UniformBase, APP.Events, function() {

    this.type = 'Matrix3fv';
    this.shaderType = 'mat3'

    this.clone = function() {
        return new APP.Uniforms.UniformMatrix3fv(
            _.clone(_.omit(this.attributes, ['value', 'program', 'location']))
        );
    }

});

APP.Uniforms.UniformMatrix4fv = Compose(APP.Uniforms.UniformBase, APP.Events, function() {

    this.type = 'Matrix4fv';
    this.shaderType = 'mat4'

    this.clone = function() {
        return new APP.Uniforms.UniformMatrix4fv(
            _.clone(_.omit(this.attributes, ['value', 'program', 'location']))
        );
    }

});

APP.Uniforms.UniformBool = Compose(APP.Uniforms.UniformBase, APP.Events, function() {

    this.type = '1i';
    this.shaderType = 'bool'

    this.clone = function() {
        return new APP.Uniforms.UniformBool(
            _.clone(_.omit(this.attributes, ['value', 'program', 'location']))
        );
    }

});

//=============================

APP.Uniforms.UniformCollection = function(params) {

    var defaults = {
        uniforms: []
    };

    var _attrs = _.extend(defaults, _.clone(params));

    Object.defineProperty(this, 'uniforms', {
        get: function() {
            return _attrs.uniforms;
        }
    });

    _attrs.lastValues = undefined;

    // Attach each uniform to the collection object directly.
    var self = this;
    _.each(this.uniforms, function(u) {
        self[u.name] = u;
    });

    // Return the collection of uniforms into one string,
    // like this :
    // uniform mat3 uMyUniform1;
    // uniform vec3 uMyUniform2;
    // etc.
    this.toString = function() {
        return _.map(this.uniforms, function(u) {
            return u.toString();
        }).join('\n');
    }

    // Return a new uniform collection based on the ones
    // provided as parameters.
    // We can pass a variable number of arguments to the function.
    // Example : uc.unionWith(uc1, uc2, uc3) is correct
    //           uc.unionWith(uc1, uc2, uc3, uc4, uc5) is also correct
    this.union = function() {
        // First concat the uniform arrays.
        // 'arguments' is a parameter passed to each call of a function
        // in Javascript.
        var args = _.toArray(arguments);
        var concatArgsUniforms = _.flatten(_.map(args, function(uc) {
            return uc.uniforms;
        }));
        var concatUniforms = this.uniforms.concat(concatArgsUniforms);
        // Second remove duplicated uniforms.
        var withoutDuplicates = _.uniq(concatUniforms, function(u) {
            return u.name;
        });
        // Clone each uniform.
        // var union = _.map(withoutDuplicates, function(u) {
        //     return u.clone();
        // });

        // Return a new collection based on the cloned uniforms.
        return new APP.Uniforms.UniformCollection({
            uniforms: withoutDuplicates // union
        });
    }

    // Return a hash containing the uniforms' names as keys,
    // and the complete name as value (with the prefix)
    this.namesToJSON = function() {
        // Invert keys and values of the object.
        // Example : a simple object like { name : 'Hello'}
        // will become : { 'Hello' : name }
        var invertedNames = _.invert(APP.Shaders.uniformNames);

        return _.reduce(this.uniforms, function(acc, u) {
            // Get the associated key of uniformNames by looking
            // at its value.
            // Example :
            // Supose u.name = 'ModelMatrix' :
            // invertedNames[u.name] -> 
            // invertedNames['ModelMatrix'] = modelMat
            acc[invertedNames[u.name]] = u.completeName;
            return acc;
        }, {});
    }

    this.clone = function() {
        return new APP.Uniforms.UniformCollection({
            uniforms: _.map(this.uniforms, function(u) {
                return u.clone();
            })
        });
    }

    // For setting multiple uniform values at a time.
    // Takes an object like this :
    // {
    //      'ModelMatrix'  : modelMat,
    //      'NormalMatrix' : normalMat
    //      etc.
    // }
    this.setValues = function(valuesObj) {
        var self = this;
        _.each(valuesObj, function(value, key) {
            if (!_.isUndefined(self[key])) {
                self[key].value = value;
            }
        });

        _attrs.lastValues = valuesObj;

        return this; // Chaining
    }

    // Work only if setValues has been used for setting uniform values.
    this.setDefaultValues = function() {
        if (!_.isUndefined(_attrs.lastValues)) {

            // Map uniforms to an object containing key/value pairs of (name, defaultValue).
            var uniformsObj = _.object(_.chain(this.uniforms)
                .map(function(u) {
                    return [u.name, u.defaultValue];
                })
                .omit(function(value, key) {
                    // Omit all pairs with undefined default value.
                    return !_.isUndefined(value);
                }).value());

            // Remove values already set.
            var toSet = _.omit(uniformsObj, _.keys(_attrs.lastValues));

            // For each value not set, assign the default value.
            _.each(toSet, function(value, key) {
                self[key].value = value;
            });

            return this; // For chaining if necessary.

        }
    }

}
