APP.Shaders = APP.Shaders || {};

var us = APP.Uniforms; // Shortcut for the object.

// Uniforms' names, without prefix.
APP.Shaders.uniformNames = {
    modelMat: 'ModelMatrix',
    viewMat: 'ViewMatrix',
    projMat: 'ProjMatrix',
    normalMat: 'NormalMatrix',
    directionalLightDirection: 'DirectionalLightDirection',
    directionalLightColor: 'DirectionalLightColor',
    ambientLightColor: 'AmbientLightColor',
    sampler: 'Sampler',
    useTextures: 'UseTextures',
    useLighting: 'UseLighting'
};

var attrPrefix = 'a'

var _anames = {
    position: 'Position',
    normal: 'Normal',
    uv: 'Uv',
    color: 'Color'
};

var attribNames = {};
_.each(_anames, function(value, key) {
    attribNames[key] = attrPrefix + value
});

APP.Shaders.attributeNames = attribNames;

//==============================================================================
//==============================================================================
// Simple attributes object

var attributes = {
    position: {
        name: attribNames.position,
        shaderType: 'vec3',
        location: undefined
    },
    normal: {
        name: attribNames.normal,
        shaderType: 'vec3',
        location: undefined
    },
    uv: {
        name: attribNames.uv,
        shaderType: 'vec2',
        location: undefined
    },
    color: {
        name: attribNames.color,
        shaderType: 'vec4',
        location: undefined
    }
};

attributes.stringTemplate = "attribute <%=attr.shaderType%> <%=attr.name%>;"
attributes.template = _.template(attributes.stringTemplate, {
    variable: 'attr'
});
attributes.toString = function() {
    var self = this;
    return _.map(['position', 'normal', 'uv', 'color'], function(prop) {
        return self.template(self[prop]);
    }).join('\n');
}
attributes.namesToJSON = function() {
    return _.mapObject(this, function(val) {
        return val.name;
    });
}
attributes.clone = function() {
    return _.mapObject(this, function(val) {
        return _.clone(val);
    })
}

//==============================================================================
//==============================================================================

// Make uniforms' and attributes' names variables immutables.
Object.freeze(attribNames);
Object.freeze(APP.Shaders.uniformNames);

var unames = APP.Shaders.uniformNames; // Shortcut.

// Common uniform variables for the vertex shaders.
APP.Shaders.commonVertexShaderUniforms = new us.UniformCollection({
    uniforms: [
        new us.UniformMatrix4fv({
            name: unames.modelMat
        }),
        new us.UniformMatrix4fv({
            name: unames.viewMat
        }),
        new us.UniformMatrix4fv({
            name: unames.projMat
        }),
        new us.UniformMatrix3fv({
            name: unames.normalMat
        })
    ]
});

// Common uniform variables for the fragment shaders.
APP.Shaders.commonFragmentShaderUniforms = new us.UniformCollection({
    uniforms: [
        new us.UniformSampler2D({
            name: unames.sampler,
            value: 0
        }),
        new us.UniformBool({
            name: unames.useTextures,
            value: false // Do not use textures by default
        })
    ]
});

// Concat vertex and frag shaders' uniforms.
var defaultShaderUniforms =
    APP.Shaders.commonVertexShaderUniforms.union(APP.Shaders.commonFragmentShaderUniforms);

// Default vertex shader source.
var getDefaultVertexShaderStringTemplate = function() {
    return $('#default-vertex-shader').html();
}

// Default fragment shader source.
var getDefaultFragmentShaderStringTemplate = function() {
    return $('#default-fragment-shader').html();
}

// Multilights vertex shader source.
var getMultilightsVertexShaderStringTemplate = function() {
    return $('#multilights-vertex-shader').html();
}

// Multilights fragment shader source.
var getMultilightsFragmentShaderStringTemplate = function() {
    return $('#multilights-fragment-shader').html();
}

// Beckmann fragment shader source.
var getBeckmannFragmentShaderStringTemplate = function() {
    return $('#beckmann-fragment-shader').html();
}

// Ward Anisotropic fragment shader source.
var getWardAnisotropicShaderStringTemplate = function() {
    return $('#wardAnisotropic-fragment-shader').html();
}

var getWaterVertexShaderSourceString = function() {
    return $('#water-vertex-shader').html();
}

//===========================================================================================
//===========================================================================================
// Multilights fragment shader

var MAX_SPOTLIGHTS = 'MAX_SPOTLIGHTS',
    MAX_DIRECTION_LIGHTS = 'MAX_DIRECTION_LIGHTS',
    MAX_POINT_LIGHTS = 'MAX_POINT_LIGHTS';

APP.Shaders.multilightsFragmentShaderUniforms = new us.UniformCollection({
    uniforms: [

        //======================================
        // Spolight uniforms

        new APP.Uniforms.Uniform3fv({
            name: 's_position',
            namePrefix: undefined,
            arrayLenConst: MAX_SPOTLIGHTS
        }),
        new APP.Uniforms.Uniform3fv({
            name: 's_direction',
            namePrefix: undefined,
            arrayLenConst: MAX_SPOTLIGHTS
        }),
        new APP.Uniforms.Uniform1f({
            name: 's_cutoff',
            namePrefix: undefined,
            arrayLenConst: MAX_SPOTLIGHTS
        }),
        new APP.Uniforms.Uniform1f({
            name: 's_outerCutoff',
            namePrefix: undefined,
            arrayLenConst: MAX_SPOTLIGHTS
        }),
        new APP.Uniforms.Uniform1f({
            name: 's_constant',
            namePrefix: undefined,
            arrayLenConst: MAX_SPOTLIGHTS
        }),
        new APP.Uniforms.Uniform1f({
            name: 's_linear',
            namePrefix: undefined,
            arrayLenConst: MAX_SPOTLIGHTS
        }),
        new APP.Uniforms.Uniform1f({
            name: 's_quadratic',
            namePrefix: undefined,
            arrayLenConst: MAX_SPOTLIGHTS
        }),
        new APP.Uniforms.Uniform3fv({
            name: 's_ambient',
            namePrefix: undefined,
            arrayLenConst: MAX_SPOTLIGHTS
        }),
        new APP.Uniforms.Uniform3fv({
            name: 's_diffuse',
            namePrefix: undefined,
            arrayLenConst: MAX_SPOTLIGHTS
        }),
        new APP.Uniforms.Uniform3fv({
            name: 's_specular',
            namePrefix: undefined,
            arrayLenConst: MAX_SPOTLIGHTS
        }),

        //======================================
        // Directional light uniforms

        new APP.Uniforms.Uniform3fv({
            name: 'd_direction',
            namePrefix: undefined,
            arrayLenConst: MAX_DIRECTION_LIGHTS
        }),
        new APP.Uniforms.Uniform3fv({
            name: 'd_ambient',
            namePrefix: undefined,
            arrayLenConst: MAX_DIRECTION_LIGHTS
        }),
        new APP.Uniforms.Uniform3fv({
            name: 'd_diffuse',
            namePrefix: undefined,
            arrayLenConst: MAX_DIRECTION_LIGHTS
        }),
        new APP.Uniforms.Uniform3fv({
            name: 'd_specular',
            namePrefix: undefined,
            arrayLenConst: MAX_DIRECTION_LIGHTS
        }),

        //======================================
        // Point light uniforms

        new APP.Uniforms.Uniform3fv({
            name: 'p_position',
            namePrefix: undefined,
            arrayLenConst: MAX_POINT_LIGHTS
        }),
        new APP.Uniforms.Uniform3fv({
            name: 'p_ambient',
            namePrefix: undefined,
            arrayLenConst: MAX_POINT_LIGHTS
        }),
        new APP.Uniforms.Uniform3fv({
            name: 'p_diffuse',
            namePrefix: undefined,
            arrayLenConst: MAX_POINT_LIGHTS
        }),
        new APP.Uniforms.Uniform3fv({
            name: 'p_specular',
            namePrefix: undefined,
            arrayLenConst: MAX_POINT_LIGHTS
        }),
        new APP.Uniforms.Uniform1f({
            name: 'p_constant',
            namePrefix: undefined,
            arrayLenConst: MAX_POINT_LIGHTS
        }),
        new APP.Uniforms.Uniform1f({
            name: 'p_linear',
            namePrefix: undefined,
            arrayLenConst: MAX_POINT_LIGHTS
        }),
        new APP.Uniforms.Uniform1f({
            name: 'p_quadratic',
            namePrefix: undefined,
            arrayLenConst: MAX_POINT_LIGHTS
        }),

        //======================================
        // Material uniforms

        new us.UniformBool({
            name: 'UseLighting',
            namePrefix: 'u',
            value: true // Use lighting by default
        }),
        new us.UniformBool({
            name: 'UseTextures',
            namePrefix: 'u',
            value: false // Do not use textures by default
        }),
        new us.UniformBool({
            name: 'UseSpecularMap',
            namePrefix: 'u',
            value: false // Do not use specular map by default
        }),
        new us.UniformSampler2D({
            name: 'texture_diffuse',
            namePrefix: undefined
        }),
        new us.UniformSampler2D({
            name: 'texture_specular',
            namePrefix: undefined
        }),
        new APP.Uniforms.Uniform3fv({
            name: 'm_ambient',
            namePrefix: undefined
        }),
        new APP.Uniforms.Uniform3fv({
            name: 'm_diffuse',
            namePrefix: undefined
        }),
        new APP.Uniforms.Uniform3fv({
            name: 'm_specular',
            namePrefix: undefined
        }),
        new APP.Uniforms.Uniform1f({
            name: 'm_shininess',
            namePrefix: undefined
        }),
		new APP.Uniforms.Uniform1f({ //Beckmann
            name: 'm_roughness',
            namePrefix: undefined
        }),
		new APP.Uniforms.Uniform1f({ //Ward Anisotropic
            name: 'm_Xroughness',
            namePrefix: undefined
        }),
		new APP.Uniforms.Uniform1f({ //Ward Anisotropic
            name: 'm_Yroughness',
            namePrefix: undefined
        }),

        //======================================
        // Others

        new APP.Uniforms.Uniform3fv({
            name: 'cameraPosition',
            namePrefix: undefined
        })

    ]
});

APP.Shaders.waterVertexShaderUniforms = new us.UniformCollection({
    uniforms: [
        new us.Uniform1f({
            name: 'waveTime',
            namePrefix: undefined
        }),
        new us.Uniform1f({
            name: 'waveWidth',
            namePrefix: undefined
        }),
        new us.Uniform1f({
            name: 'waveHeight',
            namePrefix: undefined
        })
    ]
}).union(APP.Shaders.commonVertexShaderUniforms).clone();


//===========================================================================================
//===========================================================================================

APP.uncompiledShaders = {

    defaultShader: {

        vsUniforms: APP.Shaders.commonVertexShaderUniforms,

        fsUniforms: APP.Shaders.multilightsFragmentShaderUniforms,

        vertexShaderSourceIsTemplate: true,

        fragmentShaderSourceIsTemplate: false,

        attributes: attributes,

        vertexShaderSourceGetFunc: getMultilightsVertexShaderStringTemplate,

        fragmentShaderSourceGetFunc: getMultilightsFragmentShaderStringTemplate

    },

    waterShader: {

        vsUniforms: APP.Shaders.waterVertexShaderUniforms,

        fsUniforms: APP.Shaders.multilightsFragmentShaderUniforms.clone(),

        vertexShaderSourceIsTemplate: false,

        fragmentShaderSourceIsTemplate: false,

        attributes: attributes.clone(),

        vertexShaderSourceGetFunc: getWaterVertexShaderSourceString,

        fragmentShaderSourceGetFunc: getMultilightsFragmentShaderStringTemplate   

    }

}
