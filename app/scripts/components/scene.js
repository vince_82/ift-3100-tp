APP.__Scene = Compose(function(options) {

    var defaults = {
        ambientLightColor: vec3.fromValues(0.9, 0.9, 0.9), // Almost white
        directionalLight: {
            color: vec3.fromValues(0.9, 0.9, 0.9), // Almost white
            direction: vec3.fromValues(0, -1, 0), // From the top
            specular: vec3.fromValues(1.0, 1.0, 1.0)
        }
    };

    var _attrs = APP.Utils.copyToAndUpdate(defaults, options);

    var self = this;
    _.each(_.keys(_attrs), function(prop) {
        Object.defineProperty(self, prop, {
            get: function() {
                return _attrs[prop];
            },
            set: function(value) {
                _attrs[prop] = value
            }
        });
    });

    this.parent = undefined;

    // Encapsulate a reference to the global WGL context.
    var gl = APP.WebGLContext;

    this.draw = function(args) {

        var shader = args.defaultShader;

        shader.uniforms['d_ambient'].value = this.ambientLightColor;
        shader.uniforms['d_diffuse'].value = this.directionalLight.color;
        shader.uniforms['d_specular'].value = this.directionalLight.specular;
        shader.uniforms['d_direction'].value = this.directionalLight.direction;

        // Pass lights to objects.
        args.ambientLightColor = this.ambientLightColor;
        _.extend(args, {
            directionalLight: this.directionalLight
        });

        _.invoke(this.children, 'draw', args);

        if (this.cubemap) {
            // draw skybox in last for performance reason.
            this.cubemap.draw(args);
        }

    }

});

// Scene inherits Object3D for simplification.
APP.Scene = Compose(APP.Object3D, APP.__Scene);
